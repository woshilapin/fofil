FROM docker.io/clux/muslrust:stable AS chef
RUN cargo install cargo-chef

WORKDIR /src

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder
COPY --from=planner /src/recipe.json recipe.json
# Without incremental build, 'cargo-chef' is going to be useless
ENV CARGO_INCREMENTAL=1
RUN cargo chef cook --release --target x86_64-unknown-linux-musl --recipe-path recipe.json

COPY . .
RUN cargo build --release --target x86_64-unknown-linux-musl --bin fofil

FROM scratch
COPY --from=builder /src/target/x86_64-unknown-linux-musl/release/fofil /fofil
CMD ["/fofil"]
