#![warn(
    missing_docs,
    rustdoc::missing_doc_code_examples,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    rust_2018_idioms,
    rustdoc::broken_intra_doc_links
)]
//! Fofil Statistics provides statistics about a transit data model.
//!
//! [`Statistics`] implements [`FromParser`] trait and can be provided a
//! [`Parser`] in order to have statistics about any transit data model
//! implementing [`Parser`].
//!
//! [`FromParser`]: ./trait.FromParser.html
//! [`Parser`]: ./trait.Parser.html
//! [`Statistics`]: ./struct.Statistics.html

use fofil_core::{FromParser, ObjectType, Parser, Visitor};
use std::iter::IntoIterator;

/// Structure to retain statistics about a transit model data.
#[derive(Debug, Default)]
pub struct Statistics {
    counts: std::collections::HashMap<ObjectType, usize>,
}

/// Iterator for [`Statistics`] which will iterate over the
/// different object type in order of most larger object to
/// most specific object, starting will all the referential
/// objects of transit data.
#[derive(Debug)]
pub struct StatisticsIter {
    statistics: Statistics,
    next_type_index: usize,
}

impl StatisticsIter {
    fn new(statistics: Statistics) -> Self {
        Self {
            statistics,
            next_type_index: 0,
        }
    }
}

const OBJECT_TYPE_ORDER: [ObjectType; 12] = [
    ObjectType::StopArea,
    ObjectType::StopPoint,
    ObjectType::Network,
    ObjectType::Line,
    ObjectType::Route,
    ObjectType::Equipment,
    ObjectType::Level,
    ObjectType::Calendar,
    ObjectType::JourneyPattern,
    ObjectType::JourneyStop,
    ObjectType::ScheduledJourney,
    ObjectType::ScheduledStop,
];
impl Iterator for StatisticsIter {
    type Item = (ObjectType, usize);
    fn next(&mut self) -> Option<Self::Item> {
        while self.next_type_index < OBJECT_TYPE_ORDER.len() {
            let r#type = &OBJECT_TYPE_ORDER[self.next_type_index];
            self.next_type_index += 1;
            match self.statistics.counts.get(r#type) {
                Some(count) => return Some((*r#type, *count)),
                None => continue,
            }
        }
        None
    }
}

impl IntoIterator for Statistics {
    type Item = <Self::IntoIter as Iterator>::Item;
    type IntoIter = StatisticsIter;
    fn into_iter(self) -> Self::IntoIter {
        StatisticsIter::new(self)
    }
}

struct IdleVisitor;
impl Visitor for IdleVisitor {
    type Value = ();
}

impl FromParser for Statistics {
    type Value = Statistics;
    fn from_parser<P>(mut parser: P) -> Result<Self::Value, P::Error>
    where
        P: Parser,
    {
        let mut statistics = Statistics::default();
        let idle_visitor = || IdleVisitor;
        tracing::debug!("counting calendars");
        statistics
            .counts
            .insert(ObjectType::Calendar, parser.calendars(idle_visitor).count());
        tracing::debug!("counting equipments");
        statistics.counts.insert(
            ObjectType::Equipment,
            parser.equipments(idle_visitor).count(),
        );
        tracing::debug!("counting journey pattern");
        statistics.counts.insert(
            ObjectType::JourneyPattern,
            parser.journey_patterns(idle_visitor).count(),
        );
        tracing::debug!("counting journey stop");
        statistics.counts.insert(
            ObjectType::JourneyStop,
            parser.journey_stops(idle_visitor).count(),
        );
        tracing::debug!("counting level");
        statistics
            .counts
            .insert(ObjectType::Level, parser.levels(idle_visitor).count());
        tracing::debug!("counting line");
        statistics
            .counts
            .insert(ObjectType::Line, parser.lines(idle_visitor).count());
        tracing::debug!("counting network");
        statistics
            .counts
            .insert(ObjectType::Network, parser.networks(idle_visitor).count());
        tracing::debug!("counting route");
        statistics
            .counts
            .insert(ObjectType::Route, parser.routes(idle_visitor).count());
        tracing::debug!("counting scheduled journey");
        statistics.counts.insert(
            ObjectType::ScheduledJourney,
            parser.scheduled_journeys(idle_visitor).count(),
        );
        tracing::debug!("counting scheduled stop");
        statistics.counts.insert(
            ObjectType::ScheduledStop,
            parser.scheduled_stops(idle_visitor).count(),
        );
        tracing::debug!("counting stop area");
        statistics.counts.insert(
            ObjectType::StopArea,
            parser.stop_areas(idle_visitor).count(),
        );
        tracing::debug!("counting stop point");
        statistics.counts.insert(
            ObjectType::StopPoint,
            parser.stop_points(idle_visitor).count(),
        );
        Ok(statistics)
    }
}
