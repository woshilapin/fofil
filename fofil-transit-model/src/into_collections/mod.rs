//! Module providing implementation of [`FromParser`] for
//! [`transit_model::model::Collections`].
//!
//! [`FromParser`]: ./trait.FromParser.html
//! [`transit_model::model::Collections`]: https://docs.rs/transit_model/latest/transit_model/model/struct.Collections.html

mod visitors;

use self::visitors::{
    CalendarVisitor, EquipmentVisitor, LevelVisitor, LineVisitor, NetworkVisitor, RouteVisitor,
    StopAreaVisitor, StopPointVisitor, VehicleJourneyVisitor,
};
use fofil_core::{Error, FromParser, ObjectType, Parser};
use transit_model::model::Collections;

/// Parse into a [`Collections`](transit_model::model::Collections).
#[derive(Clone, Copy, Debug)]
pub struct IntoCollections;

impl FromParser for IntoCollections {
    type Value = Collections;
    fn from_parser<P>(mut parser: P) -> Result<Self::Value, P::Error>
    where
        P: Parser,
    {
        let mut collections = Collections::default();

        // Calendars
        for calendar in parser.calendars(|| CalendarVisitor) {
            collections
                .calendars
                .push(calendar?)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(calendar) => {
                        P::Error::identifier_collision(ObjectType::Calendar, calendar.id)
                    }
                })?;
        }

        // Equipments
        for equipment in parser.equipments(|| EquipmentVisitor) {
            collections
                .equipments
                .push(equipment?)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(equipment) => {
                        P::Error::identifier_collision(ObjectType::Equipment, equipment.id)
                    }
                })?;
        }

        // Levels
        for level in parser.levels(|| LevelVisitor) {
            collections
                .levels
                .push(level?)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(level) => {
                        P::Error::identifier_collision(ObjectType::Level, level.id)
                    }
                })?;
        }

        // Line
        for line in parser.lines(|| LineVisitor) {
            collections.lines.push(line?).map_err(|error| match error {
                typed_index_collection::Error::IdentifierAlreadyExists(line) => {
                    P::Error::identifier_collision(ObjectType::Line, line.id)
                }
            })?;
        }

        // Networks
        for network in parser.networks(|| NetworkVisitor) {
            collections
                .networks
                .push(network?)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(network) => {
                        P::Error::identifier_collision(ObjectType::Network, network.id)
                    }
                })?;
        }

        // Routes
        for route in parser.routes(|| RouteVisitor) {
            collections
                .routes
                .push(route?)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(route) => {
                        P::Error::identifier_collision(ObjectType::Route, route.id)
                    }
                })?;
        }

        // Stop Areas
        for stop_area in parser.stop_areas(|| StopAreaVisitor) {
            collections
                .stop_areas
                .push(stop_area?)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(stop_area) => {
                        P::Error::identifier_collision(ObjectType::StopArea, stop_area.id)
                    }
                })?;
        }

        // Stop Points
        for stop_point in parser.stop_points(|| StopPointVisitor) {
            collections
                .stop_points
                .push(stop_point?)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(stop_point) => {
                        P::Error::identifier_collision(ObjectType::StopPoint, stop_point.id)
                    }
                })?;
        }

        // Vehicle Journeys
        let stop_points = &collections.stop_points;
        let vehicle_journeys = &mut collections.vehicle_journeys;
        let stop_time_ids = &mut collections.stop_time_ids;
        for value in parser.scheduled_journeys(|| VehicleJourneyVisitor::new(stop_points)) {
            let (vehicle_journey, new_stop_time_ids) = value?;
            vehicle_journeys
                .push(vehicle_journey)
                .map_err(|error| match error {
                    typed_index_collection::Error::IdentifierAlreadyExists(vehicle_journey) => {
                        P::Error::identifier_collision(
                            ObjectType::ScheduledJourney,
                            vehicle_journey.id,
                        )
                    }
                })?;
            stop_time_ids.extend(new_stop_time_ids);
        }

        // FIXME: do not use default objects here
        let physical_mode = transit_model::objects::PhysicalMode::default();
        tracing::trace!("physical mode '{}' created", physical_mode.id);
        collections.physical_modes = typed_index_collection::CollectionWithId::from(physical_mode);
        let commercial_mode = transit_model::objects::CommercialMode::default();
        tracing::trace!("commercial mode '{}' created", commercial_mode.id);
        collections.commercial_modes =
            typed_index_collection::CollectionWithId::from(commercial_mode);
        let dataset = transit_model::objects::Dataset::default();
        tracing::trace!("dataset '{}' created", dataset.id);
        collections.datasets = typed_index_collection::CollectionWithId::from(dataset);
        let contributor = transit_model::objects::Contributor::default();
        tracing::trace!("contributor '{}' created", contributor.id);
        collections.contributors = typed_index_collection::CollectionWithId::from(contributor);
        let company = transit_model::objects::Company::default();
        tracing::trace!("company '{}' created", company.id);
        collections.companies = typed_index_collection::CollectionWithId::from(company);

        Ok(collections)
    }
}
