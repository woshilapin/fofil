use fofil_core::{view::EquipmentView, Visitor};
use transit_model::objects::{Availability, Equipment};

pub struct EquipmentVisitor;

impl Visitor for EquipmentVisitor {
    type Value = Equipment;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_equipment<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: EquipmentView<'model>,
    {
        let id = view.id()?;
        let wheelchair_boarding = if let Some(wheelchair_boarding) = view.wheelchair_boarding()? {
            if wheelchair_boarding {
                Availability::Available
            } else {
                Availability::NotAvailable
            }
        } else {
            Availability::InformationNotAvailable
        };
        let equipment = Self::Value {
            id,
            wheelchair_boarding,
            ..Default::default()
        };
        tracing::trace!("equipment '{}' created", equipment.id);
        Ok(equipment)
    }
}
