use fofil_core::{
    view::{EquipmentView, FareZoneView, LevelView, StopAreaView, StopPointView},
    Visitor,
};
use transit_model::objects::StopPoint;

pub struct StopPointVisitor;

impl Visitor for StopPointVisitor {
    type Value = StopPoint;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_stop_point<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: StopPointView<'model>,
    {
        let id = view.id()?;
        let name = view.name()?;
        let stop_area_id = view.stop_area()?.id()?;
        let visible = view.visible()?;
        let fare_zone_id = view
            .fare_zone()?
            .map(|fare_zone_view| fare_zone_view.id())
            .transpose()?;
        let platform_code = view.platform_code()?;
        let level_id = view
            .level()?
            .map(|level_view| level_view.id())
            .transpose()?;
        let equipment_id = view
            .equipment()?
            .map(|equipment_view| equipment_view.id())
            .transpose()?;
        let stop_point = Self::Value {
            id,
            name,
            visible,
            stop_area_id,
            equipment_id,
            fare_zone_id,
            level_id,
            platform_code,
            ..Default::default()
        };
        tracing::trace!("stop point '{}' created", stop_point.id);
        Ok(stop_point)
    }
}
