use fofil_core::{
    view::{CalendarView, DateView},
    Error, Visitor,
};
use transit_model::objects::Calendar;

pub struct CalendarVisitor;

impl Visitor for CalendarVisitor {
    type Value = Calendar;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_calendar<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: CalendarView<'model>,
    {
        let id = view.id()?;
        let dates = view
            .dates()?
            .map(|date_view| {
                use std::convert::TryFrom;
                let year = date_view.year()?;
                let year = i32::try_from(year).map_err(|_| {
                    V::Error::invalid_type(
                        std::any::type_name::<i32>().to_string(),
                        year.to_string(),
                    )
                })?;
                let month = date_view.month()?;
                let month = u32::try_from(month).map_err(|_| {
                    V::Error::invalid_type(
                        std::any::type_name::<u32>().to_string(),
                        month.to_string(),
                    )
                })?;
                let day = date_view.day()?;
                let day = u32::try_from(day).map_err(|_| {
                    V::Error::invalid_type(
                        std::any::type_name::<u32>().to_string(),
                        day.to_string(),
                    )
                })?;
                let date = chrono::NaiveDate::from_ymd(year, month, day);
                Ok(date)
            })
            .collect::<Result<_, _>>()?;
        let calendar = Self::Value { id, dates };
        tracing::trace!("calendar '{}' created", calendar.id);
        Ok(calendar)
    }
}
