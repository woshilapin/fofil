use fofil_core::{view::LevelView, Error, Visitor};
use rust_decimal::prelude::ToPrimitive;
use transit_model::objects::Level;

pub struct LevelVisitor;

impl Visitor for LevelVisitor {
    type Value = Level;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_level<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: LevelView<'model>,
    {
        let id = view.id()?;
        let floor = view.floor()?;
        let level_index = floor.to_f32().ok_or_else(|| {
            V::Error::invalid_type(std::any::type_name::<f32>().to_string(), floor.to_string())
        })?;
        let level_name = view.name()?;
        let level = Self::Value {
            id,
            level_index,
            level_name,
        };
        tracing::trace!("level '{}' created", level.id);
        Ok(level)
    }
}
