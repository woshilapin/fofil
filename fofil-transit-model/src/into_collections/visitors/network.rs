use fofil_core::{view::NetworkView, Visitor};
use transit_model::objects::Network;

pub struct NetworkVisitor;

impl Visitor for NetworkVisitor {
    type Value = Network;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_network<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: NetworkView<'model>,
    {
        let id = view.id()?;
        let name = view.name()?;
        let network = Self::Value {
            id,
            name,
            ..Default::default()
        };
        tracing::trace!("network '{}' created", network.id);
        Ok(network)
    }
}
