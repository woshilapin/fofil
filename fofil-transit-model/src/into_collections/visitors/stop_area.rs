use fofil_core::{view::StopAreaView, Visitor};
use transit_model::objects::StopArea;

pub struct StopAreaVisitor;

impl Visitor for StopAreaVisitor {
    type Value = StopArea;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_stop_area<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: StopAreaView<'model>,
    {
        let id = view.id()?;
        let name = view.name()?;
        let stop_area = Self::Value {
            id,
            name,
            ..Default::default()
        };
        tracing::trace!("stop area '{}' created", stop_area.id);
        Ok(stop_area)
    }
}
