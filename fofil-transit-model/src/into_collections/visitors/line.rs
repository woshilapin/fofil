use fofil_core::{
    view::{LineView, NetworkView},
    Visitor,
};
use transit_model::objects::Line;

pub struct LineVisitor;

impl Visitor for LineVisitor {
    type Value = Line;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_line<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: LineView<'model>,
    {
        let id = view.id()?;
        let name = view.name()?;
        let network_id = view.network()?.id()?;
        let line = Self::Value {
            id,
            name,
            network_id,
            ..Default::default()
        };
        tracing::trace!("line '{}' created", line.id);
        Ok(line)
    }
}
