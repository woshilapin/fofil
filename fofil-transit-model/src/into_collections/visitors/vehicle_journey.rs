use fofil_core::{
    view::{
        CalendarView, JourneyPatternView, JourneyStopView, RouteView, ScheduledJourneyView,
        ScheduledStopView, StopPointView,
    },
    Error, Visitor,
};
use std::collections::HashMap;
use transit_model::objects::{StopPoint, StopTime, Time, VehicleJourney};
use typed_index_collection::CollectionWithId;

pub struct VehicleJourneyVisitor<'stops> {
    stops: &'stops CollectionWithId<StopPoint>,
}
type StopTimeIdentifiers = HashMap<(String, u32), String>;

impl<'stops> VehicleJourneyVisitor<'stops> {
    pub fn new(stops: &'stops CollectionWithId<StopPoint>) -> Self {
        Self { stops }
    }
}

impl<'stops> Visitor for VehicleJourneyVisitor<'stops> {
    type Value = (VehicleJourney, StopTimeIdentifiers);

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_scheduled_journey<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: ScheduledJourneyView<'model>,
    {
        let id = view.id()?;
        let route_id = view.route()?.id()?;
        let service_id = view.calendar()?.id()?;
        let journey_pattern_id = view.journey_pattern()?.id().ok();
        let stop_times = view
            .scheduled_stops()?
            .map(|scheduled_stop_view| {
                let journey_stop_view = scheduled_stop_view.journey_stop()?;
                let stop_point_id = journey_stop_view.stop_point()?.id()?;
                let sequence = journey_stop_view.sequence()?;
                let sequence: u32 = std::convert::TryInto::try_into(sequence).map_err(|_| {
                    V::Error::invalid_type(
                        std::any::type_name::<u32>().to_string(),
                        sequence.to_string(),
                    )
                })?;
                let stop_point_idx = self.stops.get_idx(&stop_point_id).ok_or_else(|| {
                    V::Error::identifier_not_found(
                        fofil_core::ObjectType::ScheduledStop,
                        stop_point_id,
                    )
                })?;
                let stop_time = StopTime {
                    stop_point_idx,
                    sequence,
                    arrival_time: Time::default(),
                    departure_time: Time::default(),
                    boarding_duration: 0,
                    alighting_duration: 0,
                    pickup_type: 0,
                    drop_off_type: 0,
                    local_zone_id: None,
                    precision: None,
                };
                tracing::trace!("stop time created");
                Ok(stop_time)
            })
            .collect::<Result<_, _>>()?;
        let vehicle_journey = VehicleJourney {
            id,
            route_id,
            service_id,
            stop_times,
            journey_pattern_id,
            ..Default::default()
        };
        tracing::trace!("vehicle journey '{}' created", vehicle_journey.id);
        let mut stop_time_ids = StopTimeIdentifiers::new();
        for scheduled_stop_view in view.scheduled_stops()? {
            if let Ok(stop_time_id) = scheduled_stop_view.id() {
                let vehicle_journey_id = vehicle_journey.id.clone();
                let journey_stop_view = scheduled_stop_view.journey_stop()?;
                let sequence: usize = journey_stop_view.sequence()?;
                let sequence: u32 = std::convert::TryFrom::try_from(sequence).map_err(|_| {
                    V::Error::invalid_type(
                        std::any::type_name::<u32>().to_string(),
                        sequence.to_string(),
                    )
                })?;
                stop_time_ids.insert((vehicle_journey_id, sequence), stop_time_id);
            }
        }
        Ok((vehicle_journey, stop_time_ids))
    }
}
