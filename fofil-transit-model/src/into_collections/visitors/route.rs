use fofil_core::{
    view::{LineView, RouteView},
    Visitor,
};
use transit_model::objects::Route;

pub struct RouteVisitor;

impl Visitor for RouteVisitor {
    type Value = Route;

    #[tracing::instrument(
    	level = "error",
    	skip(self, view),
        fields(
            id = %view.id()?
        )
    )]
    fn visit_route<'model, V>(self, view: V) -> Result<Self::Value, V::Error>
    where
        V: RouteView<'model>,
    {
        let id = view.id()?;
        let name = view.name()?;
        let line_id = view.line()?.id()?;
        let route = Self::Value {
            id,
            name,
            line_id,
            ..Default::default()
        };
        tracing::trace!("route '{}' created", route.id);
        Ok(route)
    }
}
