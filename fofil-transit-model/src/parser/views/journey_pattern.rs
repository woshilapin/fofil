use crate::parser::{
    journey_pattern::JourneyPattern, JourneyStopView, Parser, RouteView, ScheduledJourneyView,
};
use dyn_iter::DynIter;
use fofil_core::{Error, ObjectType};

#[derive(Debug)]
pub struct JourneyPatternView<'model> {
    parser: &'model Parser<'model>,
    journey_pattern: &'model JourneyPattern<'model>,
}

impl<'model> JourneyPatternView<'model> {
    pub fn new(
        parser: &'model Parser<'model>,
        journey_pattern: &'model JourneyPattern<'model>,
    ) -> Self {
        JourneyPatternView {
            parser,
            journey_pattern,
        }
    }
}

impl<'model> fofil_core::view::JourneyPatternView<'model> for JourneyPatternView<'model> {
    type Error = fofil_core::FofilError;
    type RouteView = RouteView<'model>;
    type ScheduledJourneyView = ScheduledJourneyView<'model>;
    type ScheduledJourneyViewIter = DynIter<'model, Self::ScheduledJourneyView>;
    type JourneyStopView = JourneyStopView<'model>;
    type JourneyStopViewIter = DynIter<'model, Self::JourneyStopView>;

    #[tracing::instrument(
        name = "view_journey_pattern",
        level = "error",
        skip(self),
        fields(
            id = ?self.journey_pattern.id(),
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{:?}'", self.journey_pattern.id());
        self.journey_pattern
            .id()
            .map(ToOwned::to_owned)
            .ok_or_else(|| {
                tracing::debug!("no id found");
                Self::Error::no_such_property(
                    ObjectType::JourneyPattern,
                    format!(
                        "for vehicle journeys {}",
                        self.journey_pattern.vehicle_journeys().fold(
                            String::new(),
                            |mut acc, vehicle_journey| {
                                if acc.is_empty() {
                                    acc = vehicle_journey.id.clone();
                                } else {
                                    acc += ", ";
                                    acc += vehicle_journey.id.as_str();
                                }
                                acc
                            }
                        )
                    ),
                    "journey_pattern_id".to_string(),
                )
            })
    }
    #[tracing::instrument(
        name = "view_journey_pattern",
        level = "error",
        skip(self),
        fields(
            id = ?self.journey_pattern.id(),
        )
    )]
    fn route(&self) -> Result<Self::RouteView, Self::Error> {
        let routes = self
            .parser
            .collections
            .routes
            .values()
            .filter(|route| route.id == self.journey_pattern.route_id())
            .collect::<Vec<_>>();
        if routes.len() == 1 {
            let view = Self::RouteView::new(self.parser, routes[0]);
            tracing::trace!("view of route '{}'", routes[0].id);
            Ok(view)
        } else if routes.is_empty() {
            tracing::debug!("no route found");
            Err(Self::Error::identifier_not_found(
                ObjectType::Route,
                self.journey_pattern.route_id().to_owned(),
            ))
        } else {
            tracing::debug!("multiple routes found");
            Err(Self::Error::identifier_collision(
                ObjectType::Route,
                self.journey_pattern.route_id().to_owned(),
            ))
        }
    }
    #[tracing::instrument(
        name = "view_journey_pattern",
        level = "error",
        skip(self),
        fields(
            id = ?self.journey_pattern.id(),
        )
    )]
    // ALLOW: When trying to follow the proposed fix, generates a compiler error
    // FIXME: Most likely, this is either a `rustc`, `cargo` or `clippy` bug but
    // it might need some consequent work to produce an actionable issue report.
    #[allow(clippy::needless_collect)]
    fn scheduled_journeys(&self) -> Result<Self::ScheduledJourneyViewIter, Self::Error> {
        let views: Vec<Self::ScheduledJourneyView> = self
            .journey_pattern
            .vehicle_journeys()
            .map(|vehicle_journey| Self::ScheduledJourneyView::new(self.parser, vehicle_journey))
            .collect();
        let views = DynIter::<'model, ScheduledJourneyView<'model>>::new(views.into_iter());
        tracing::trace!("view of scheduled journeys");
        Ok(views)
    }
    #[tracing::instrument(
        name = "view_journey_pattern",
        level = "error",
        skip(self),
        fields(
            id = ?self.journey_pattern.id(),
        )
    )]
    fn journey_stops(&self) -> Result<Self::JourneyStopViewIter, Self::Error> {
        let parser = self.parser;
        let vehicle_journey = self.journey_pattern.base_vehicle_journey();
        let iter = vehicle_journey
            .stop_times
            .iter()
            .map(move |stop_time| Self::JourneyStopView::new(parser, vehicle_journey, stop_time));
        let views = Self::JourneyStopViewIter::new(iter);
        tracing::trace!("view of journey stops");
        Ok(views)
    }
}
