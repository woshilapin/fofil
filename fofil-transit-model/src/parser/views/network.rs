use crate::parser::{LineView, Parser};
use dyn_iter::DynIter;
use transit_model::objects::Network;

#[derive(Debug)]
pub struct NetworkView<'model> {
    parser: &'model Parser<'model>,
    network: &'model Network,
}

impl<'model> NetworkView<'model> {
    pub fn new(parser: &'model Parser<'model>, network: &'model Network) -> Self {
        NetworkView { parser, network }
    }
}

impl<'model> fofil_core::view::NetworkView<'model> for NetworkView<'model> {
    type Error = fofil_core::FofilError;
    type LineView = LineView<'model>;
    type LineViewIter = DynIter<'model, Self::LineView>;

    #[tracing::instrument(
        name = "view_network",
        level = "error",
        skip(self),
        fields(
            id = %self.network.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.network.id);
        Ok(self.network.id.clone())
    }
    #[tracing::instrument(
        name = "view_network",
        level = "error",
        skip(self),
        fields(
            id = %self.network.id,
        )
    )]
    fn name(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of name '{}'", self.network.name);
        Ok(self.network.name.clone())
    }
    #[tracing::instrument(
        name = "view_network",
        level = "error",
        skip(self),
        fields(
            id = %self.network.id,
        )
    )]
    fn lines(&self) -> Result<Self::LineViewIter, Self::Error> {
        let parser = self.parser;
        let network = self.network;
        let iter = parser
            .collections
            .lines
            .values()
            .filter(move |line| line.network_id == network.id)
            .map(move |line| Self::LineView::new(parser, line));
        let views = Self::LineViewIter::new(iter);
        tracing::trace!("view of lines");
        Ok(views)
    }
}
