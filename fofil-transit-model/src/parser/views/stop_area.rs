use crate::parser::{Parser, StopPointView};
use transit_model::objects::StopArea;

#[derive(Debug)]
pub struct StopAreaView<'model> {
    parser: &'model Parser<'model>,
    stop_area: &'model StopArea,
}

impl<'model> StopAreaView<'model> {
    pub fn new(parser: &'model Parser<'model>, stop_area: &'model StopArea) -> Self {
        StopAreaView { parser, stop_area }
    }
}

impl<'model> fofil_core::view::StopAreaView<'model> for StopAreaView<'model> {
    type Error = fofil_core::FofilError;
    type StopPointView = StopPointView<'model>;

    #[tracing::instrument(
        name = "view_stop_area",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_area.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.stop_area.id);
        Ok(self.stop_area.id.clone())
    }
    #[tracing::instrument(
        name = "view_stop_area",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_area.id,
        )
    )]
    fn name(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of name '{}'", self.stop_area.name);
        Ok(self.stop_area.name.clone())
    }
    #[tracing::instrument(
        name = "view_stop_area",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_area.id,
        )
    )]
    fn stop_points(&self) -> Result<Vec<Self::StopPointView>, Self::Error> {
        let views = self
            .parser
            .collections
            .stop_points
            .values()
            .filter(|stop_point| stop_point.stop_area_id == self.stop_area.id)
            .map(|stop_point| Self::StopPointView::new(self.parser, stop_point))
            .collect::<Vec<_>>();
        tracing::trace!("view of stop points");
        Ok(views)
    }
}
