use crate::parser::{NetworkView, Parser, RouteView};
use dyn_iter::DynIter;
use fofil_core::{Error, ObjectType};
use transit_model::objects::Line;

#[derive(Debug)]
pub struct LineView<'model> {
    parser: &'model Parser<'model>,
    line: &'model Line,
}

impl<'model> LineView<'model> {
    pub fn new(parser: &'model Parser<'model>, line: &'model Line) -> Self {
        LineView { parser, line }
    }
}

impl<'model> fofil_core::view::LineView<'model> for LineView<'model> {
    type Error = fofil_core::FofilError;
    type NetworkView = NetworkView<'model>;
    type RouteView = RouteView<'model>;
    type RouteViewIter = DynIter<'model, Self::RouteView>;

    #[tracing::instrument(
        name = "view_line",
        level = "error",
        skip(self),
        fields(
            id = %self.line.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.line.id);
        Ok(self.line.id.clone())
    }
    #[tracing::instrument(
        name = "view_line",
        level = "error",
        skip(self),
        fields(
            id = %self.line.id,
        )
    )]
    fn name(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of name '{}'", self.line.name);
        Ok(self.line.name.clone())
    }
    /// Get a view on the parent network.
    #[tracing::instrument(
        name = "view_line",
        level = "error",
        skip(self),
        fields(
            id = %self.line.id,
        )
    )]
    fn network(&self) -> Result<Self::NetworkView, Self::Error> {
        let networks = self
            .parser
            .collections
            .networks
            .values()
            .filter(|network| network.id == self.line.network_id)
            .collect::<Vec<_>>();
        if networks.len() == 1 {
            let view = Self::NetworkView::new(self.parser, networks[0]);
            tracing::trace!("view of network '{}'", networks[0].id);
            Ok(view)
        } else if networks.is_empty() {
            tracing::debug!("no network found");
            Err(Self::Error::identifier_not_found(
                ObjectType::Network,
                self.line.network_id.clone(),
            ))
        } else {
            tracing::debug!("multiple networks found");
            Err(Self::Error::identifier_collision(
                ObjectType::Network,
                self.line.network_id.clone(),
            ))
        }
    }
    #[tracing::instrument(
        name = "view_line",
        level = "error",
        skip(self),
        fields(
            id = %self.line.id,
        )
    )]
    fn routes(&self) -> Result<Self::RouteViewIter, Self::Error> {
        let parser = self.parser;
        let line = self.line;
        let iter = parser
            .collections
            .routes
            .values()
            .filter(move |route| route.line_id == line.id)
            .map(move |route| Self::RouteView::new(parser, route));
        let views = Self::RouteViewIter::new(iter);
        tracing::trace!("view of routes");
        Ok(views)
    }
}
