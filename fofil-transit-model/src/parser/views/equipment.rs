use transit_model::objects::{Availability, Equipment};

#[derive(Debug)]
pub struct EquipmentView<'model> {
    equipment: &'model Equipment,
}

impl<'model> EquipmentView<'model> {
    pub fn new(equipment: &'model Equipment) -> Self {
        EquipmentView { equipment }
    }
}

impl<'model> fofil_core::view::EquipmentView<'model> for EquipmentView<'model> {
    type Error = fofil_core::FofilError;

    #[tracing::instrument(
        name = "view_equipment",
        level = "error",
        skip(self),
        fields(
            id = %self.equipment.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.equipment.id);
        Ok(self.equipment.id.clone())
    }
    #[tracing::instrument(
        name = "view_equipment",
        level = "error",
        skip(self),
        fields(
            id = %self.equipment.id,
        )
    )]
    fn wheelchair_boarding(&self) -> Result<Option<bool>, Self::Error> {
        let wheelchair_boarding = match self.equipment.wheelchair_boarding {
            Availability::Available => Some(true),
            Availability::NotAvailable => Some(false),
            Availability::InformationNotAvailable => None,
        };
        tracing::trace!("view of wheelchair boarding '{:?}'", wheelchair_boarding);
        Ok(wheelchair_boarding)
    }
}
