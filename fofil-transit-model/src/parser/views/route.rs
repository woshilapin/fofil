use crate::parser::{JourneyPatternView, LineView, Parser};
use dyn_iter::DynIter;
use fofil_core::{Error, ObjectType};
use transit_model::objects::Route;

#[derive(Debug)]
pub struct RouteView<'model> {
    parser: &'model Parser<'model>,
    route: &'model Route,
}

impl<'model> RouteView<'model> {
    pub fn new(parser: &'model Parser<'model>, route: &'model Route) -> Self {
        RouteView { parser, route }
    }
}

impl<'model> fofil_core::view::RouteView<'model> for RouteView<'model> {
    type Error = fofil_core::FofilError;
    type LineView = LineView<'model>;
    type JourneyPatternView = JourneyPatternView<'model>;
    type JourneyPatternViewIter = DynIter<'model, Self::JourneyPatternView>;

    #[tracing::instrument(
        name = "view_route",
        level = "error",
        skip(self),
        fields(
            id = %self.route.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.route.id);
        Ok(self.route.id.clone())
    }
    #[tracing::instrument(
        name = "view_route",
        level = "error",
        skip(self),
        fields(
            id = %self.route.id,
        )
    )]
    fn name(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of name '{}'", self.route.name);
        Ok(self.route.name.clone())
    }
    #[tracing::instrument(
        name = "view_route",
        level = "error",
        skip(self),
        fields(
            id = %self.route.id,
        )
    )]
    fn line(&self) -> Result<Self::LineView, Self::Error> {
        let lines = self
            .parser
            .collections
            .lines
            .values()
            .filter(|line| line.id == self.route.line_id)
            .collect::<Vec<_>>();
        if lines.len() == 1 {
            let view = Self::LineView::new(self.parser, lines[0]);
            tracing::trace!("view of line '{}'", lines[0].id);
            Ok(view)
        } else if lines.is_empty() {
            tracing::debug!("no line found");
            Err(Self::Error::identifier_not_found(
                ObjectType::Line,
                self.route.line_id.clone(),
            ))
        } else {
            tracing::debug!("multiple lines found");
            Err(Self::Error::identifier_collision(
                ObjectType::Line,
                self.route.line_id.clone(),
            ))
        }
    }
    #[tracing::instrument(
        name = "view_route",
        level = "error",
        skip(self),
        fields(
            id = %self.route.id,
        )
    )]
    fn journey_patterns(&self) -> Result<Self::JourneyPatternViewIter, Self::Error> {
        let parser = self.parser;
        let route = self.route;
        let iter = parser
            .collections_journey_patterns
            .iter()
            .filter(move |journey_pattern| journey_pattern.route_id() == route.id)
            .map(move |journey_pattern| Self::JourneyPatternView::new(parser, journey_pattern));
        let views = Self::JourneyPatternViewIter::new(iter);
        tracing::trace!("view of journey patterns");
        Ok(views)
    }
}
