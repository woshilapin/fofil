use crate::parser::{JourneyPatternView, Parser, ScheduledStopView, StopPointView};
use dyn_iter::DynIter;
use fofil_core::{Error, ObjectType};
use transit_model::objects::{StopTime, VehicleJourney};

#[derive(Debug)]
pub struct JourneyStopView<'model> {
    parser: &'model Parser<'model>,
    vehicle_journey: &'model VehicleJourney,
    stop_time: &'model StopTime,
}

impl<'model> JourneyStopView<'model> {
    pub fn new(
        parser: &'model Parser<'model>,
        vehicle_journey: &'model VehicleJourney,
        stop_time: &'model StopTime,
    ) -> Self {
        JourneyStopView {
            parser,
            vehicle_journey,
            stop_time,
        }
    }
}

impl<'model> fofil_core::view::JourneyStopView<'model> for JourneyStopView<'model> {
    type Error = fofil_core::FofilError;
    type StopPointView = StopPointView<'model>;
    type JourneyPatternView = JourneyPatternView<'model>;
    type ScheduledStopView = ScheduledStopView<'model>;
    type ScheduledStopViewIter = DynIter<'model, Self::ScheduledStopView>;

    #[tracing::instrument(
        name = "view_journey_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("no id found");
        Err(Self::Error::no_such_property(
            ObjectType::JourneyStop,
            format!(
                "{} [in vehicle journey '{}']",
                self.stop_time.sequence, self.vehicle_journey.id
            ),
            "id".to_string(),
        ))
    }
    #[tracing::instrument(
        name = "view_journey_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn stop_point(&self) -> Result<Self::StopPointView, Self::Error> {
        let view = StopPointView::new(
            self.parser,
            &self.parser.collections.stop_points[self.stop_time.stop_point_idx],
        );
        tracing::trace!(
            "view of stop point '{}'",
            self.parser.collections.stop_points[self.stop_time.stop_point_idx].id
        );
        Ok(view)
    }
    #[tracing::instrument(
        name = "view_journey_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn sequence(&self) -> Result<usize, Self::Error> {
        tracing::trace!("view of sequence");
        std::convert::TryFrom::try_from(self.stop_time.sequence).map_err(|_| {
            tracing::debug!("failed to convert '{}' into usize", self.stop_time.sequence);
            Self::Error::invalid_type("usize".to_string(), "u32".to_string())
        })
    }
    #[tracing::instrument(
        name = "view_journey_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn journey_pattern(&self) -> Result<Self::JourneyPatternView, Self::Error> {
        let journey_pattern = self
            .parser
            .collections_journey_patterns
            .iter()
            .find(|journey_pattern| {
                journey_pattern
                    .vehicle_journeys()
                    .any(|vehicle_journey| vehicle_journey.id == self.vehicle_journey.id)
            })
            .ok_or_else(|| {
                tracing::debug!("no scheduled journey found");
                Self::Error::identifier_not_found(
                    ObjectType::ScheduledJourney,
                    self.vehicle_journey.id.clone(),
                )
            })?;
        let view = JourneyPatternView::new(self.parser, journey_pattern);
        tracing::trace!("view of journey pattern '{:?}'", journey_pattern.id());
        Ok(view)
    }
    #[tracing::instrument(
        name = "view_journey_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn scheduled_stops(&self) -> Result<Self::ScheduledStopViewIter, Self::Error> {
        todo!("Cannot implement this until we really have all the vehicle journeys associated with the current journey pattern.")
    }
}
