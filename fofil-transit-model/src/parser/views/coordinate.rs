use transit_model::objects::Coord;

#[derive(Debug)]
pub struct CoordinateView {
    coordinates: Coord,
}

impl From<Coord> for CoordinateView {
    fn from(coordinates: Coord) -> Self {
        CoordinateView { coordinates }
    }
}

impl fofil_core::view::CoordinateView<'_> for CoordinateView {
    type Error = fofil_core::FofilError;

    #[tracing::instrument(name = "view_coordinate", level = "error", skip(self))]
    fn latitude(&self) -> Result<f64, Self::Error> {
        tracing::trace!("view of latitude '{}'", self.coordinates.lat);
        Ok(self.coordinates.lat)
    }
    #[tracing::instrument(name = "view_coordinate", level = "error", skip(self))]
    fn longitude(&self) -> Result<f64, Self::Error> {
        tracing::trace!("view of longitude '{}'", self.coordinates.lon);
        Ok(self.coordinates.lon)
    }
}
