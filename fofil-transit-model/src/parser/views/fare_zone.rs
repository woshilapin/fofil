#[derive(Debug)]
pub struct FareZoneView {
    fare_zone_id: String,
}

impl From<String> for FareZoneView {
    fn from(fare_zone_id: String) -> Self {
        FareZoneView { fare_zone_id }
    }
}

impl fofil_core::view::FareZoneView<'_> for FareZoneView {
    type Error = fofil_core::FofilError;

    #[tracing::instrument(
        name = "view_fare_zone",
        level = "error",
        skip(self),
        fields(
            id = %self.fare_zone_id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.fare_zone_id);
        Ok(self.fare_zone_id.clone())
    }
}
