use dyn_iter::DynIter;
use transit_model::objects::Calendar;

#[derive(Debug)]
pub struct CalendarView<'model> {
    calendar: &'model Calendar,
}

impl<'model> CalendarView<'model> {
    pub fn new(calendar: &'model Calendar) -> Self {
        CalendarView { calendar }
    }
}

impl<'model> fofil_core::view::CalendarView<'model> for CalendarView<'model> {
    type Error = fofil_core::FofilError;
    type DateView = chrono::NaiveDate;
    type DateViewIter = DynIter<'model, Self::DateView>;

    #[tracing::instrument(
        name = "view_calendar",
        level = "trace",
        skip(self),
        fields(
            id = %self.calendar.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.calendar.id);
        Ok(self.calendar.id.clone())
    }

    #[tracing::instrument(
        name = "view_calendar",
        level = "trace",
        skip(self),
        fields(
            id = %self.calendar.id,
        )
    )]
    fn dates(&self) -> Result<Self::DateViewIter, Self::Error> {
        tracing::trace!("view of dates in calendar '{}'", self.calendar.id);
        Ok(DynIter::new(self.calendar.dates.iter().copied()))
    }
}
