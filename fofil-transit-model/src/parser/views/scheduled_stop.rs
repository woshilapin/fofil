use crate::parser::{JourneyStopView, Parser, ScheduledJourneyView};
use fofil_core::{Error, ObjectType};
use transit_model::objects::{StopTime, VehicleJourney};

#[derive(Debug)]
pub struct ScheduledStopView<'model> {
    parser: &'model Parser<'model>,
    vehicle_journey: &'model VehicleJourney,
    stop_time: &'model StopTime,
}

impl<'model> ScheduledStopView<'model> {
    pub fn new(
        parser: &'model Parser<'model>,
        vehicle_journey: &'model VehicleJourney,
        stop_time: &'model StopTime,
    ) -> Self {
        ScheduledStopView {
            parser,
            vehicle_journey,
            stop_time,
        }
    }
}

impl<'model> fofil_core::view::ScheduledStopView<'model> for ScheduledStopView<'model> {
    type Error = fofil_core::FofilError;
    type ScheduledJourneyView = ScheduledJourneyView<'model>;
    type JourneyStopView = JourneyStopView<'model>;

    #[tracing::instrument(
        name = "view_scheduled_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id");
        self.parser
            .collections
            .stop_time_ids
            .get(&(self.vehicle_journey.id.clone(), self.stop_time.sequence))
            .cloned()
            .ok_or_else(|| {
                tracing::debug!("no id found");
                Self::Error::no_such_property(
                    ObjectType::ScheduledStop,
                    format!(
                        "{} [in vehicle journey {}]",
                        self.stop_time.sequence, self.vehicle_journey.id
                    ),
                    "stop_time_id".to_string(),
                )
            })
    }
    #[tracing::instrument(
        name = "view_scheduled_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn scheduled_journey(&self) -> Result<Self::ScheduledJourneyView, Self::Error> {
        let view = ScheduledJourneyView::new(self.parser, self.vehicle_journey);
        tracing::trace!("view of scheduled journey");
        Ok(view)
    }
    #[tracing::instrument(
        name = "view_scheduled_stop",
        level = "error",
        skip(self),
        fields(
            vehicle_journey.id = %self.vehicle_journey.id,
            stop_time.sequence = %self.stop_time.sequence,
        )
    )]
    fn journey_stop(&self) -> Result<Self::JourneyStopView, Self::Error> {
        let view = JourneyStopView::new(self.parser, self.vehicle_journey, self.stop_time);
        tracing::trace!("view of scheduled stop");
        Ok(view)
    }
}
