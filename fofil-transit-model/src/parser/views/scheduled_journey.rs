use crate::parser::{CalendarView, JourneyPatternView, Parser, ScheduledStopView};
use dyn_iter::DynIter;
use fofil_core::{Error, ObjectType};
use transit_model::objects::VehicleJourney;

#[derive(Debug)]
pub struct ScheduledJourneyView<'model> {
    parser: &'model Parser<'model>,
    vehicle_journey: &'model VehicleJourney,
}

impl<'model> ScheduledJourneyView<'model> {
    pub fn new(parser: &'model Parser<'model>, vehicle_journey: &'model VehicleJourney) -> Self {
        ScheduledJourneyView {
            parser,
            vehicle_journey,
        }
    }
}

impl<'model> fofil_core::view::ScheduledJourneyView<'model> for ScheduledJourneyView<'model> {
    type Error = fofil_core::FofilError;
    type JourneyPatternView = JourneyPatternView<'model>;
    type CalendarView = CalendarView<'model>;
    type ScheduledStopView = ScheduledStopView<'model>;
    type ScheduledStopViewIter = DynIter<'model, Self::ScheduledStopView>;

    #[tracing::instrument(
        name = "view_scheduled_journey",
        level = "error",
        skip(self),
        fields(
            id = %self.vehicle_journey.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.vehicle_journey.id);
        Ok(self.vehicle_journey.id.clone())
    }
    #[tracing::instrument(
        name = "view_scheduled_journey",
        level = "error",
        skip(self),
        fields(
            id = %self.vehicle_journey.id,
        )
    )]
    fn journey_pattern(&self) -> Result<Self::JourneyPatternView, Self::Error> {
        let journey_pattern = self
            .parser
            .collections_journey_patterns
            .iter()
            .find(|journey_pattern| {
                journey_pattern
                    .vehicle_journeys()
                    .any(|vehicle_journey| vehicle_journey.id == self.vehicle_journey.id)
            })
            .ok_or_else(|| {
                tracing::debug!("no journey pattern found");
                Self::Error::identifier_not_found(
                    ObjectType::ScheduledJourney,
                    self.vehicle_journey.id.clone(),
                )
            })?;
        let view = JourneyPatternView::new(self.parser, journey_pattern);
        tracing::trace!("view of journey pattern '{:?}'", journey_pattern.id());
        Ok(view)
    }
    #[tracing::instrument(
        name = "view_scheduled_journey",
        level = "error",
        skip(self),
        fields(
            id = %self.vehicle_journey.id,
        )
    )]
    fn calendar(&self) -> Result<Self::CalendarView, Self::Error> {
        tracing::trace!("view of calendar '{}'", self.vehicle_journey.service_id);
        self.parser
            .collections
            .calendars
            .get(&self.vehicle_journey.service_id)
            .map(Self::CalendarView::new)
            .ok_or_else(|| {
                tracing::debug!("no calendar found");
                Self::Error::identifier_not_found(
                    ObjectType::Calendar,
                    self.vehicle_journey.service_id.clone(),
                )
            })
    }
    #[tracing::instrument(
        name = "view_scheduled_journey",
        level = "error",
        skip(self),
        fields(
            id = %self.vehicle_journey.id,
        )
    )]
    fn scheduled_stops(&self) -> Result<Self::ScheduledStopViewIter, Self::Error> {
        let parser = self.parser;
        let vehicle_journey = self.vehicle_journey;
        let iter = vehicle_journey
            .stop_times
            .iter()
            .map(move |stop_time| ScheduledStopView::new(parser, vehicle_journey, stop_time));
        let views = Self::ScheduledStopViewIter::new(iter);
        tracing::trace!("view of scheduled stops");
        Ok(views)
    }
}
