use crate::parser::{CoordinateView, EquipmentView, FareZoneView, LevelView, Parser, StopAreaView};
use chrono_tz::Tz;
use fofil_core::{Error, ObjectType};
use transit_model::objects::StopPoint;

#[derive(Debug)]
pub struct StopPointView<'model> {
    parser: &'model Parser<'model>,
    stop_point: &'model StopPoint,
}

impl<'model> StopPointView<'model> {
    pub fn new(parser: &'model Parser<'model>, stop_point: &'model StopPoint) -> Self {
        StopPointView { parser, stop_point }
    }
}

impl<'model> fofil_core::view::StopPointView<'model> for StopPointView<'model> {
    type Error = fofil_core::FofilError;
    type CoordinateView = CoordinateView;
    type EquipmentView = EquipmentView<'model>;
    type FareZoneView = FareZoneView;
    type LevelView = LevelView<'model>;
    type StopAreaView = StopAreaView<'model>;

    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.stop_point.id);
        Ok(self.stop_point.id.clone())
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn name(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of name '{}'", self.stop_point.name);
        Ok(self.stop_point.name.clone())
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn stop_area(&self) -> Result<Self::StopAreaView, Self::Error> {
        let stop_areas = self
            .parser
            .collections
            .stop_areas
            .values()
            .filter(|stop_area| stop_area.id == self.stop_point.stop_area_id)
            .collect::<Vec<_>>();
        if stop_areas.len() == 1 {
            let view = Self::StopAreaView::new(self.parser, stop_areas[0]);
            tracing::trace!("view of stop area '{}'", stop_areas[0].id);
            Ok(view)
        } else if stop_areas.is_empty() {
            tracing::debug!("no stop area found");
            Err(Self::Error::identifier_not_found(
                ObjectType::StopArea,
                self.stop_point.stop_area_id.clone(),
            ))
        } else {
            tracing::debug!("multiple stop areas found");
            Err(Self::Error::identifier_collision(
                ObjectType::StopArea,
                self.stop_point.stop_area_id.clone(),
            ))
        }
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn visible(&self) -> Result<bool, Self::Error> {
        tracing::trace!("view of visible '{}'", self.stop_point.visible);
        Ok(self.stop_point.visible)
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn coordinates(&self) -> Result<Self::CoordinateView, Self::Error> {
        tracing::trace!("view of coordinates");
        Ok(self.stop_point.coord.into())
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn fare_zone(&self) -> Result<Option<Self::FareZoneView>, Self::Error> {
        let view = self
            .stop_point
            .fare_zone_id
            .as_ref()
            .cloned()
            .map(FareZoneView::from);
        tracing::trace!("view of fare zone '{:?}'", self.stop_point.fare_zone_id);
        Ok(view)
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn timezone(&self) -> Result<Option<Tz>, Self::Error> {
        let view = self.stop_point.timezone.as_ref().copied();
        tracing::trace!("view of timezone '{:?}'", self.stop_point.timezone);
        Ok(view)
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn equipment(&self) -> Result<Option<Self::EquipmentView>, Self::Error> {
        tracing::trace!("view of equipment '{:?}'", self.stop_point.equipment_id);
        self.stop_point
            .equipment_id
            .as_ref()
            .map(|equipment_id| {
                self.parser
                    .collections
                    .equipments
                    .get(equipment_id)
                    .map(EquipmentView::new)
                    .ok_or_else(|| {
                        tracing::debug!("no equipment found");
                        Self::Error::identifier_not_found(
                            ObjectType::Equipment,
                            equipment_id.clone(),
                        )
                    })
            })
            .transpose()
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn level(&self) -> Result<Option<Self::LevelView>, Self::Error> {
        tracing::trace!("view of level '{:?}'", self.stop_point.level_id);
        self.stop_point
            .level_id
            .as_ref()
            .map(|level_id| {
                self.parser
                    .collections
                    .levels
                    .get(level_id)
                    .map(LevelView::new)
                    .ok_or_else(|| {
                        tracing::debug!("no level found");
                        Self::Error::identifier_not_found(ObjectType::Level, level_id.clone())
                    })
            })
            .transpose()
    }
    #[tracing::instrument(
        name = "view_stop_point",
        level = "error",
        skip(self),
        fields(
            id = %self.stop_point.id,
        )
    )]
    fn platform_code(&self) -> Result<Option<String>, Self::Error> {
        tracing::trace!(
            "view of platform code '{:?}'",
            self.stop_point.platform_code
        );
        Ok(self.stop_point.platform_code.as_ref().cloned())
    }
}
