use fofil_core::Error;
use rust_decimal::{prelude::FromPrimitive, Decimal};
use transit_model::objects::Level;

#[derive(Debug)]
pub struct LevelView<'model> {
    level: &'model Level,
}

impl<'model> LevelView<'model> {
    pub fn new(level: &'model Level) -> Self {
        LevelView { level }
    }
}

impl<'model> fofil_core::view::LevelView<'model> for LevelView<'model> {
    type Error = fofil_core::FofilError;

    #[tracing::instrument(
        name = "view_level",
        level = "error",
        skip(self),
        fields(
            id = %self.level.id,
        )
    )]
    fn id(&self) -> Result<String, Self::Error> {
        tracing::trace!("view of id '{}'", self.level.id);
        Ok(self.level.id.clone())
    }
    #[tracing::instrument(
        name = "view_level",
        level = "error",
        skip(self),
        fields(
            id = %self.level.id,
        )
    )]
    fn floor(&self) -> Result<Decimal, Self::Error> {
        tracing::trace!("view of floor '{}'", self.level.level_index);
        Decimal::from_f32(self.level.level_index).ok_or_else(|| {
            tracing::debug!(
                "failed to convert '{}' into Decimal",
                self.level.level_index
            );
            Self::Error::invalid_type(
                std::any::type_name::<Decimal>().to_string(),
                self.level.level_index.to_string(),
            )
        })
    }
    #[tracing::instrument(
        name = "view_level",
        level = "error",
        skip(self),
        fields(
            id = %self.level.id,
        )
    )]
    fn name(&self) -> Result<Option<String>, Self::Error> {
        tracing::trace!("view of name '{:?}'", self.level.level_name);
        Ok(self.level.level_name.as_ref().cloned())
    }
}
