use self::journey_pattern::JourneyPatterns;
use fofil_core::{FofilError, Visitor};
use transit_model::{model::Collections, objects as ntm};

mod journey_pattern;
mod views;

pub use views::*;

/// [`Parser`](fofil_core::Parser) for the transit data model in
/// [`transit_model`].
#[derive(Debug)]
pub struct Parser<'model> {
    // Model
    collections: &'model Collections,
    collections_journey_patterns: JourneyPatterns<'model>,
    // Keep track of last parsed object
    calendars: std::slice::Iter<'model, ntm::Calendar>,
    equipments: std::slice::Iter<'model, ntm::Equipment>,
    journey_patterns: usize,
    levels: std::slice::Iter<'model, ntm::Level>,
    lines: std::slice::Iter<'model, ntm::Line>,
    networks: std::slice::Iter<'model, ntm::Network>,
    routes: std::slice::Iter<'model, ntm::Route>,
    stop_areas: std::slice::Iter<'model, ntm::StopArea>,
    stop_points: std::slice::Iter<'model, ntm::StopPoint>,
    vehicle_journeys: std::slice::Iter<'model, ntm::VehicleJourney>,
}

impl<'model> Parser<'model> {
    /// Creates a new [`Parser`](fofil_core::Parser) from a
    /// [`transit_model::model::Collections`].
    #[must_use]
    pub fn new(collections: &'model Collections) -> Self {
        Parser {
            collections,
            collections_journey_patterns: JourneyPatterns::new(
                collections.vehicle_journeys.values(),
                &journey_pattern::Configuration::default(),
            ),
            calendars: collections.calendars.values(),
            equipments: collections.equipments.values(),
            journey_patterns: 0,
            levels: collections.levels.values(),
            lines: collections.lines.values(),
            networks: collections.networks.values(),
            routes: collections.routes.values(),
            vehicle_journeys: collections.vehicle_journeys.values(),
            stop_areas: collections.stop_areas.values(),
            stop_points: collections.stop_points.values(),
        }
    }
}

impl<'model> fofil_core::Parser for Parser<'model> {
    type Error = FofilError;

    fn next_calendar<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.calendars
            .next()
            .map(CalendarView::new)
            .map(|view| visitor.visit_calendar(view))
    }

    fn next_equipment<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.equipments
            .next()
            .map(EquipmentView::new)
            .map(|view| visitor.visit_equipment(view))
    }

    fn next_journey_pattern<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        let index = self.journey_patterns;
        self.journey_patterns += 1;
        self.collections_journey_patterns
            .get(index)
            .map(|journey_pattern| JourneyPatternView::new(self, journey_pattern))
            .map(|view| visitor.visit_journey_pattern(view))
    }

    fn next_journey_stop<V>(&mut self, _visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        None
    }

    fn next_level<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.levels
            .next()
            .map(LevelView::new)
            .map(|view| visitor.visit_level(view))
    }

    fn next_line<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.lines
            .next()
            .map(|line| LineView::new(self, line))
            .map(|view| visitor.visit_line(view))
    }

    fn next_network<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.networks
            .next()
            .map(|network| NetworkView::new(self, network))
            .map(|view| visitor.visit_network(view))
    }

    fn next_route<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.routes
            .next()
            .map(|route| RouteView::new(self, route))
            .map(|view| visitor.visit_route(view))
    }

    fn next_scheduled_journey<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.vehicle_journeys
            .next()
            .map(|vehicle_journey| ScheduledJourneyView::new(self, vehicle_journey))
            .map(|view| visitor.visit_scheduled_journey(view))
    }

    fn next_scheduled_stop<V>(&mut self, _visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        None
    }

    fn next_stop_area<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.stop_areas
            .next()
            .map(|stop_area| StopAreaView::new(self, stop_area))
            .map(|view| visitor.visit_stop_area(view))
    }

    fn next_stop_point<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor,
    {
        self.stop_points
            .next()
            .map(|stop_point| StopPointView::new(self, stop_point))
            .map(|view| visitor.visit_stop_point(view))
    }
}
