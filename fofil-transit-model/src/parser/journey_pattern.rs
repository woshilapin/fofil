use transit_model::objects::{StopTime, VehicleJourney};

#[derive(Debug)]
pub struct Configuration {
    journey_pattern_id: bool,
    drop_off: bool,
    pick_up: bool,
}
impl std::default::Default for Configuration {
    fn default() -> Self {
        Self {
            journey_pattern_id: true,
            drop_off: false,
            pick_up: false,
        }
    }
}

#[derive(Debug)]
pub struct JourneyStop<'model> {
    stop_time: &'model StopTime,
}
impl<'model> std::convert::From<&'model StopTime> for JourneyStop<'model> {
    fn from(stop_time: &'model StopTime) -> Self {
        Self { stop_time }
    }
}
impl<'model> std::ops::Deref for JourneyStop<'model> {
    type Target = &'model StopTime;
    fn deref(&self) -> &Self::Target {
        &self.stop_time
    }
}

/// `JourneyPattern` is composed of:
/// - the list of `JourneyStop` that defines it
/// - the list of `VehicleJourney` following this pattern
///
/// Note that only `journey_stops` is used to implement `PartialEq`, `Eq`,
/// `Hash`.
///
/// Important: There is only one way to build a `JourneyPattern`, from a
/// reference to `VehicleJourney` (see `From<&VehicleJourney>`).  This ensures
/// that `JourneyPattern` always have at least one `VehicleJourney`.
#[derive(Debug)]
pub struct JourneyPattern<'model> {
    journey_stops: Vec<JourneyStop<'model>>,
    id: Option<&'model str>,
    route_id: &'model str,
    vehicle_journeys: Vec<&'model VehicleJourney>,
}

impl<'model> From<&'model VehicleJourney> for JourneyPattern<'model> {
    fn from(vehicle_journey: &'model VehicleJourney) -> Self {
        let journey_stops = vehicle_journey
            .stop_times
            .iter()
            .map(JourneyStop::from)
            .collect();
        let route_id = &vehicle_journey.route_id;
        JourneyPattern {
            journey_stops,
            id: vehicle_journey.journey_pattern_id.as_deref(),
            route_id,
            vehicle_journeys: vec![vehicle_journey],
        }
    }
}

impl<'model> std::ops::Deref for JourneyPattern<'model> {
    type Target = Vec<JourneyStop<'model>>;
    fn deref(&self) -> &Self::Target {
        &self.journey_stops
    }
}

impl JourneyPattern<'_> {
    fn try_merge(&mut self, other: Self, configuration: &Configuration) -> Result<(), Self> {
        if self.journey_stops.len() != other.journey_stops.len() {
            return Err(other);
        }
        if configuration.journey_pattern_id && self.id != other.id {
            return Err(other);
        }
        for (stop_a, stop_b) in self.journey_stops.iter().zip(&other.journey_stops) {
            if stop_a.stop_point_idx != stop_b.stop_point_idx {
                return Err(other);
            }
            if configuration.pick_up && stop_a.pickup_type != stop_b.pickup_type {
                return Err(other);
            }
            if configuration.drop_off && stop_a.drop_off_type != stop_b.drop_off_type {
                return Err(other);
            }
        }
        self.vehicle_journeys.extend(other.vehicle_journeys);
        Ok(())
    }
}

impl<'model> JourneyPattern<'model> {
    pub fn id(&self) -> Option<&'model str> {
        self.id
    }
    pub fn route_id(&self) -> &'model str {
        self.route_id
    }
    pub fn base_vehicle_journey(&self) -> &'model VehicleJourney {
        // A journey pattern cannot be built without at least one
        // [`VehicleJourney`]
        self.vehicle_journeys[0]
    }
    pub fn vehicle_journeys(&self) -> std::slice::Iter<'_, &'model VehicleJourney> {
        self.vehicle_journeys.iter()
    }
}

#[derive(Debug)]
pub struct JourneyPatterns<'model> {
    journey_patterns: Vec<JourneyPattern<'model>>,
}
impl<'model> JourneyPatterns<'model> {
    pub fn new(
        vehicle_journeys: std::slice::Iter<'model, VehicleJourney>,
        configuration: &Configuration,
    ) -> Self {
        let journey_patterns = vehicle_journeys.map(JourneyPattern::from).fold(
            Vec::<JourneyPattern<'model>>::new(),
            |mut journey_patterns, mut journey_pattern| {
                // FIXME: Simplify the implementation
                for jp in &mut journey_patterns {
                    journey_pattern = match jp.try_merge(journey_pattern, configuration) {
                        Ok(()) => return journey_patterns,
                        Err(journey_pattern) => journey_pattern,
                    }
                }
                journey_patterns.push(journey_pattern);
                journey_patterns
            },
        );
        JourneyPatterns { journey_patterns }
    }
}
impl<'model> std::ops::Deref for JourneyPatterns<'model> {
    type Target = Vec<JourneyPattern<'model>>;
    fn deref(&self) -> &Self::Target {
        &self.journey_patterns
    }
}

#[cfg(test)]
mod tests {
    use super::{Configuration, JourneyPatterns};
    use transit_model::objects::{StopPoint, StopTime, Time, VehicleJourney};
    use typed_index_collection::{CollectionWithId, Idx};

    fn stop_time(
        stop_point_idx: Idx<StopPoint>,
        sequence: u32,
        (pickup_type, drop_off_type): (u8, u8),
    ) -> StopTime {
        StopTime {
            stop_point_idx,
            sequence,
            arrival_time: Time::new(0, 0, 0),
            departure_time: Time::new(0, 0, 0),
            boarding_duration: 0,
            alighting_duration: 0,
            pickup_type,
            drop_off_type,
            local_zone_id: None,
            precision: None,
        }
    }

    fn stop_points() -> CollectionWithId<StopPoint> {
        CollectionWithId::new(vec![
            StopPoint {
                id: "sp1".to_string(),
                ..Default::default()
            },
            StopPoint {
                id: "sp2".to_string(),
                ..Default::default()
            },
            StopPoint {
                id: "sp3".to_string(),
                ..Default::default()
            },
            StopPoint {
                id: "sp4".to_string(),
                ..Default::default()
            },
        ])
        .unwrap()
    }

    #[test]
    fn two_journeys_one_pattern() {
        let stop_points = stop_points();
        let sp1_idx = stop_points.get_idx("sp1").unwrap();
        let sp2_idx = stop_points.get_idx("sp2").unwrap();
        let vehicle_journeys = CollectionWithId::new(vec![
            VehicleJourney {
                id: "vj1".to_string(),
                stop_times: vec![stop_time(sp1_idx, 0, (0, 0)), stop_time(sp2_idx, 1, (0, 0))],
                route_id: "route1".to_string(),
                ..Default::default()
            },
            VehicleJourney {
                id: "vj2".to_string(),
                stop_times: vec![stop_time(sp1_idx, 0, (0, 0)), stop_time(sp2_idx, 1, (0, 0))],
                route_id: "route1".to_string(),
                ..Default::default()
            },
        ])
        .unwrap();
        let journey_patterns =
            JourneyPatterns::new(vehicle_journeys.values(), &Configuration::default());
        let mut journey_patterns_iter = journey_patterns.iter();
        let journey_pattern = journey_patterns_iter.next().unwrap();
        assert_eq!(journey_pattern.len(), 2);
        assert_eq!(stop_points[journey_pattern[0].stop_point_idx].id, "sp1");
        assert_eq!(stop_points[journey_pattern[1].stop_point_idx].id, "sp2");
        assert!(journey_patterns_iter.next().is_none());
        let mut vehicle_journeys = journey_pattern.vehicle_journeys();
        assert_eq!(vehicle_journeys.next().unwrap().id, "vj1");
        assert_eq!(vehicle_journeys.next().unwrap().id, "vj2");
        assert_eq!(vehicle_journeys.next(), None);
        let route_id = journey_pattern.route_id();
        assert_eq!(route_id, "route1");
    }

    #[test]
    fn two_journeys_two_patterns() {
        let stop_points = stop_points();
        let sp1_idx = stop_points.get_idx("sp1").unwrap();
        let sp2_idx = stop_points.get_idx("sp2").unwrap();
        let vehicle_journeys = CollectionWithId::new(vec![
            VehicleJourney {
                id: "vj1".to_string(),
                stop_times: vec![stop_time(sp1_idx, 0, (0, 0)), stop_time(sp2_idx, 1, (0, 0))],
                route_id: "route1".to_string(),
                ..Default::default()
            },
            VehicleJourney {
                id: "vj2".to_string(),
                stop_times: vec![stop_time(sp2_idx, 0, (0, 0)), stop_time(sp1_idx, 1, (0, 0))],
                route_id: "route2".to_string(),
                ..Default::default()
            },
        ])
        .unwrap();
        let journey_patterns =
            JourneyPatterns::new(vehicle_journeys.values(), &Configuration::default());
        let mut journey_patterns_iter = journey_patterns.iter();
        let journey_pattern = journey_patterns_iter.next().unwrap();
        assert_eq!(journey_pattern.len(), 2);
        assert_eq!(stop_points[journey_pattern[0].stop_point_idx].id, "sp1");
        assert_eq!(stop_points[journey_pattern[1].stop_point_idx].id, "sp2");
        let mut vehicle_journeys = journey_pattern.vehicle_journeys();
        assert_eq!(vehicle_journeys.next().unwrap().id, "vj1");
        assert_eq!(vehicle_journeys.next(), None);
        let route_id = journey_pattern.route_id();
        assert_eq!(route_id, "route1");

        let journey_pattern = journey_patterns_iter.next().unwrap();
        assert_eq!(journey_pattern.len(), 2);
        assert_eq!(stop_points[journey_pattern[0].stop_point_idx].id, "sp2");
        assert_eq!(stop_points[journey_pattern[1].stop_point_idx].id, "sp1");
        assert!(journey_patterns_iter.next().is_none());
        let mut vehicle_journeys = journey_pattern.vehicle_journeys();
        assert_eq!(vehicle_journeys.next().unwrap().id, "vj2");
        assert_eq!(vehicle_journeys.next(), None);
        let route_id = journey_pattern.route_id();
        assert_eq!(route_id, "route2");
    }

    #[test]
    fn two_journeys_one_pattern_with_different_id_and_drop_off() {
        let stop_points = stop_points();
        let sp1_idx = stop_points.get_idx("sp1").unwrap();
        let sp2_idx = stop_points.get_idx("sp2").unwrap();
        let vehicle_journeys = CollectionWithId::new(vec![
            VehicleJourney {
                id: "vj1".to_string(),
                stop_times: vec![stop_time(sp1_idx, 0, (0, 0)), stop_time(sp2_idx, 1, (0, 1))],
                journey_pattern_id: Some("some_id".to_string()),
                route_id: "route1".to_string(),
                ..Default::default()
            },
            VehicleJourney {
                id: "vj2".to_string(),
                stop_times: vec![stop_time(sp1_idx, 0, (0, 1)), stop_time(sp2_idx, 1, (0, 0))],
                route_id: "route1".to_string(),
                ..Default::default()
            },
        ])
        .unwrap();
        // Configuration ignores `journey_pattern_id` and `drop_off`.
        let configuration = Configuration {
            journey_pattern_id: false,
            pick_up: true,
            drop_off: false,
        };
        let journey_patterns = JourneyPatterns::new(vehicle_journeys.values(), &configuration);
        let mut journey_patterns_iter = journey_patterns.iter();
        let journey_pattern = journey_patterns_iter.next().unwrap();
        assert_eq!(journey_pattern.len(), 2);
        assert_eq!(stop_points[journey_pattern[0].stop_point_idx].id, "sp1");
        assert_eq!(stop_points[journey_pattern[1].stop_point_idx].id, "sp2");
        assert!(journey_patterns_iter.next().is_none());
        let mut vehicle_journeys = journey_pattern.vehicle_journeys();
        assert_eq!(vehicle_journeys.next().unwrap().id, "vj1");
        assert_eq!(vehicle_journeys.next().unwrap().id, "vj2");
        assert_eq!(vehicle_journeys.next(), None);
        let route_id = journey_pattern.route_id();
        assert_eq!(route_id, "route1");
    }
}
