#![warn(
    missing_docs,
    rustdoc::missing_doc_code_examples,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    rust_2018_idioms,
    rustdoc::broken_intra_doc_links
)]
//! Providing [`Parser`] and [`FromParser`] for transit data model of
//! [`transit_model`].
//!
//! [`FromParser`]: ./trait.FromParser.html
//! [`Parser`]: ./trait.Parser.html
//! [`transit_model`]: https://docs.rs/transit_model/

mod into_collections;
mod parser;

pub use into_collections::IntoCollections;
pub use parser::Parser;

#[cfg(test)]
mod tests {
    use super::*;
    use approx::assert_relative_eq;
    use fofil_core::{FofilError, FromParser};
    use transit_model::{model::Collections, objects::*};
    use typed_index_collection::CollectionWithId;

    fn collections() -> Collections {
        let mut collections = Collections::default();
        collections.calendars = CollectionWithId::from(Calendar {
            id: "calendar_1".to_string(),
            dates: std::collections::BTreeSet::new(),
        });
        collections.equipments = CollectionWithId::from(Equipment {
            id: "equipment_1".to_string(),
            wheelchair_boarding: Availability::Available,
            ..Default::default()
        });
        collections.levels = CollectionWithId::from(Level {
            id: "level_1".to_string(),
            level_index: 1.5_f32,
            level_name: Some("Level 1".to_string()),
        });
        collections.lines = CollectionWithId::from(Line {
            id: "line_id".to_string(),
            name: "Line 1".to_string(),
            network_id: "network_id".to_string(),
            ..Default::default()
        });
        collections.networks = CollectionWithId::from(Network {
            id: "network_id".to_string(),
            name: "Network 1".to_string(),
            ..Default::default()
        });
        collections.routes = CollectionWithId::from(Route {
            id: "route_id".to_string(),
            name: "Route 1".to_string(),
            line_id: "line_id".to_string(),
            ..Default::default()
        });
        collections.stop_areas = CollectionWithId::from(StopArea {
            id: "stop_area_1".to_string(),
            name: "Stop Area 1".to_string(),
            ..Default::default()
        });
        collections.stop_points = CollectionWithId::new(vec![
            StopPoint {
                id: "stop_point_1".to_string(),
                name: "Stop Point 1".to_string(),
                stop_area_id: "stop_area_1".to_string(),
                visible: true,
                coord: Coord { lat: 1.0, lon: 2.0 },
                fare_zone_id: Some(1.to_string()),
                platform_code: Some("Porte E".to_string()),
                level_id: Some("level_1".to_string()),
                equipment_id: Some("equipment_1".to_string()),
                ..Default::default()
            },
            StopPoint {
                id: "stop_point_2".to_string(),
                name: "Stop Point 2".to_string(),
                stop_area_id: "stop_area_1".to_string(),
                visible: false,
                coord: Coord { lat: 3.0, lon: 4.0 },
                fare_zone_id: Some(2.to_string()),
                level_id: None,
                ..Default::default()
            },
        ])
        .expect("Expect to not fail on inserting stop points with different identifiers.");
        collections.vehicle_journeys = CollectionWithId::new(vec![
            VehicleJourney {
                id: "vehicle_journey_1".to_string(),
                route_id: "route_id".to_string(),
                service_id: "calendar_1".to_string(),
                short_name: Some("Vehicle Journey 1".to_string()),
                headsign: Some("To the moon".to_string()),
                journey_pattern_id: Some("journey_pattern_1".to_string()),
                stop_times: vec![
                    StopTime {
                        stop_point_idx: collections
                            .stop_points
                            .get_idx("stop_point_1")
                            .expect("Expect stop point 'stop_point_1' to exist."),
                        sequence: 0,
                        arrival_time: Time::new(7, 50, 0),
                        departure_time: Time::new(8, 0, 0),
                        boarding_duration: 120,
                        alighting_duration: 60,
                        pickup_type: 0,
                        drop_off_type: 0,
                        local_zone_id: None,
                        precision: None,
                    },
                    StopTime {
                        stop_point_idx: collections
                            .stop_points
                            .get_idx("stop_point_2")
                            .expect("Expect stop point 'stop_point_2' to exist."),
                        sequence: 1,
                        arrival_time: Time::new(8, 50, 0),
                        departure_time: Time::new(9, 0, 0),
                        boarding_duration: 120,
                        alighting_duration: 60,
                        pickup_type: 0,
                        drop_off_type: 0,
                        local_zone_id: None,
                        precision: None,
                    },
                ],
                ..Default::default()
            },
            VehicleJourney {
                id: "vehicle_journey_2".to_string(),
                route_id: "route_id".to_string(),
                service_id: "calendar_1".to_string(),
                short_name: Some("Vehicle Journey 2".to_string()),
                stop_times: vec![
                    StopTime {
                        stop_point_idx: collections
                            .stop_points
                            .get_idx("stop_point_2")
                            .expect("Expect stop point 'stop_point_2' to exist."),
                        sequence: 0,
                        arrival_time: Time::new(19, 50, 0),
                        departure_time: Time::new(20, 0, 0),
                        boarding_duration: 120,
                        alighting_duration: 60,
                        pickup_type: 0,
                        drop_off_type: 0,
                        local_zone_id: None,
                        precision: None,
                    },
                    StopTime {
                        stop_point_idx: collections
                            .stop_points
                            .get_idx("stop_point_1")
                            .expect("Expect stop point 'stop_point_1' to exist."),
                        sequence: 1,
                        arrival_time: Time::new(20, 50, 0),
                        departure_time: Time::new(21, 0, 0),
                        boarding_duration: 120,
                        alighting_duration: 60,
                        pickup_type: 0,
                        drop_off_type: 0,
                        local_zone_id: None,
                        precision: None,
                    },
                ],
                ..Default::default()
            },
        ])
        .expect("Expect to not fail on inserting vehicle journeys with different identifiers.");
        collections.stop_time_ids.insert(
            ("vehicle_journey_1".to_string(), 0),
            "stop_time_1_0".to_string(),
        );
        collections.stop_time_ids.insert(
            ("vehicle_journey_1".to_string(), 1),
            "stop_time_1_1".to_string(),
        );
        collections
    }

    #[test]
    fn equipments() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        let equipment = model
            .equipments
            .get("equipment_1")
            .expect("Expect equipment 'equipment_1' to exist.");
        assert_eq!(equipment.wheelchair_boarding, Availability::Available);
    }

    #[test]
    fn journey_patterns() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        let vehicle_journey = model
            .vehicle_journeys
            .get("vehicle_journey_1")
            .expect("Expect vehicle journey 'vehicle_journey_1' to exist.");
        let journey_pattern_id = vehicle_journey
            .journey_pattern_id
            .as_ref()
            .expect("Expect vehicle journey 'vehicle_journey_1' to have a 'journey_pattern_id'.");
        assert_eq!(journey_pattern_id, "journey_pattern_1");
        let vehicle_journey = model
            .vehicle_journeys
            .get("vehicle_journey_2")
            .expect("Expect vehicle journey 'vehicle_journey_2' to exist.");

        assert_eq!(vehicle_journey.journey_pattern_id, None);
    }

    #[test]
    fn levels() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        let level = model
            .levels
            .get("level_1")
            .expect("Expect level 'level_1' to exist.");
        assert_relative_eq!(level.level_index, 1.5_f32);
        let level_name = level
            .level_name
            .as_ref()
            .expect("Expect level 'level_1' to have a 'level_name'.");
        assert_eq!(level_name, "Level 1");
    }

    #[test]
    fn level_invalid_floor() {
        let mut collections = collections();
        collections
            .levels
            .get_mut("level_1")
            .expect("Expect level 'level_1' to exist.")
            .level_index = std::f32::NAN;
        let parser = Parser::new(&collections);
        let error = IntoCollections::from_parser(parser).expect_err("Expect conversion to fail.");

        assert_eq!(
            error,
            FofilError::InvalidType(
                "rust_decimal::decimal::Decimal".to_string(),
                "NaN".to_string()
            )
        );
    }

    #[test]
    fn lines() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        let line = model
            .lines
            .get("line_id")
            .expect("Expect line 'line_id' to exist.");
        assert_eq!(&line.name, "Line 1");
        assert_eq!(&line.network_id, "network_id");
    }

    #[test]
    fn networks() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        let network = model
            .networks
            .get("network_id")
            .expect("Expect network 'network_id' to exist.");
        assert_eq!(&network.name, "Network 1");
    }

    #[test]
    fn routes() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        let route = model
            .routes
            .get("route_id")
            .expect("Expect route 'route_id' to exist.");
        assert_eq!(&route.name, "Route 1");
    }

    #[test]
    fn stop_points() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        let stop_area = model
            .stop_areas
            .get("stop_area_1")
            .expect("Expect stop area 'stop_area_1' to exist.");
        assert_eq!(&stop_area.name, "Stop Area 1");

        let stop_point = model
            .stop_points
            .get("stop_point_1")
            .expect("Expect stop point 'stop_point_1' to exist.");
        assert_eq!(&stop_point.name, "Stop Point 1");
        assert_eq!(&stop_point.stop_area_id, "stop_area_1");
        assert!(stop_point.visible);
        let fare_zone_id = stop_point
            .fare_zone_id
            .as_ref()
            .expect("Expect stop point 'stop_point_1' to have a 'fare_zone_id'.");
        assert_eq!(fare_zone_id, "1");
        let platform_code = stop_point
            .platform_code
            .as_ref()
            .expect("Expect stop point 'stop_point_1' to have a 'platform_code'.");
        assert_eq!(platform_code, "Porte E");
        let level_id = stop_point
            .level_id
            .as_ref()
            .expect("Expect stop point 'stop_point_1' to have a 'level_id'.");
        assert_eq!(level_id, "level_1");
        let equipment_id = stop_point
            .equipment_id
            .as_ref()
            .expect("Expect stop point 'stop_point_1' to have a 'equipment_id'.");
        assert_eq!(equipment_id, "equipment_1");

        let stop_point = model
            .stop_points
            .get("stop_point_2")
            .expect("Expect stop point 'stop_point_2' to exist.");
        assert_eq!(&stop_point.name, "Stop Point 2");
        assert_eq!(&stop_point.stop_area_id, "stop_area_1");
        assert!(!stop_point.visible);
        let fare_zone_id = stop_point
            .fare_zone_id
            .as_ref()
            .expect("Expect stop point 'stop_point_2' to have a 'fare_zone_id'.");
        assert_eq!(fare_zone_id, "2");
        assert_eq!(stop_point.platform_code, None);
        assert_eq!(stop_point.level_id, None);
    }

    #[test]
    fn stop_times() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        assert_eq!(model.stop_time_ids.len(), 2);
        let stop_time_id = model
            .stop_time_ids
            .get(&("vehicle_journey_1".to_string(), 0))
            .expect("Expect stop time 0 of 'vehicle_journey_1' to have an identifier.");
        assert_eq!(stop_time_id, "stop_time_1_0");
        let stop_time_id = model
            .stop_time_ids
            .get(&("vehicle_journey_1".to_string(), 1))
            .expect("Expect stop time 1 of 'vehicle_journey_1' to have an identifier.");
        assert_eq!(stop_time_id, "stop_time_1_1");
    }

    #[test]
    fn vehicle_journeys() {
        let collections = collections();
        let parser = Parser::new(&collections);
        let model = IntoCollections::from_parser(parser)
            .expect("Expect conversion of collections to succeed.");

        assert!(model.vehicle_journeys.get("vehicle_journey_1").is_some());
        assert!(model.vehicle_journeys.get("vehicle_journey_2").is_some());
    }
}
