//! `fofil` is a binary providing an easy interface for Fofil ecosystem allowing
//! you to parse, convert and do all sorts of things on transit data.
//!
//! The following sub-command exists:
//! - `help`
//! - `completion`
//! - `convert`
//! - `stats`
//!
//! ## `completion`
//! This subcommand creates shell completions script for different shells.
//!
//! ```sh
//! fofil completion bash
//! ```
//!
//! ## `convert`
//! This subcommand convert transit data from one format to another format (see
//! [`TransitDataFormat`] for supported formats). For example, if you simply
//! want to parse NTFS format, you can do the following
//!
//! ```sh
//! fofil convert --from ntfs --input ./in-ntfs --to ntfs --output ./out-ntfs
//! ```
//!
//! ## `stats`
//! This subcommand calculates statistics about a transit data model. For
//! example, you can get statistics on a NTFS model with the following
//!
//! ```sh
//! fofil stats --from ntfs --input ./in-ntfs
//! ```
#![warn(
    missing_docs,
    rustdoc::missing_doc_code_examples,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    rust_2018_idioms,
    rustdoc::broken_intra_doc_links
)]

mod cli;
pub use cli::TransitDataFormat;

use clap::{CommandFactory, Parser};
use eyre::{Report as EyreReport, Result, WrapErr};
use fofil_core::FromParser;

#[tracing::instrument(level = "error")]
fn completion(cli::CompletionCommand { shell }: &cli::CompletionCommand) {
    use clap_complete::{generator::generate, shells};
    use cli::Shell;

    tracing::info!("generate completion scripts");

    let mut app = cli::Command::command();
    let mut fd = std::io::stdout();
    match *shell {
        Shell::Bash => generate(shells::Bash, &mut app, "fofil", &mut fd),
        Shell::Elvish => generate(shells::Elvish, &mut app, "fofil", &mut fd),
        Shell::Fish => generate(shells::Fish, &mut app, "fofil", &mut fd),
        Shell::PowerShell => generate(shells::PowerShell, &mut app, "fofil", &mut fd),
        Shell::Zsh => generate(shells::Zsh, &mut app, "fofil", &mut fd),
    };
}

#[tracing::instrument(
    level = "error",
    skip(convert_cmd),
    fields(
        from = %convert_cmd.parser_config.from,
        to = %convert_cmd.to,
    )
)]
fn convert(convert_cmd: &cli::ConvertCommand) -> Result<()> {
    use cli::TransitDataFormat;

    tracing::info!("convert transit data");

    let model;
    let parser = match convert_cmd.parser_config.from {
        #[cfg(feature = "ntfs")]
        TransitDataFormat::Ntfs => {
            model = transit_model::ntfs::read(convert_cmd.parser_config.input.as_path())
                .map_err(EyreReport::msg)
                .wrap_err("Failed to read input NTFS data.")?;
            fofil_transit_model::Parser::new(&model)
        }
    };
    match convert_cmd.to {
        #[cfg(feature = "ntfs")]
        TransitDataFormat::Ntfs => {
            use chrono::{Datelike, FixedOffset, TimeZone, Timelike, Utc};
            use fofil_transit_model::IntoCollections;
            use transit_model::{model::Model, ntfs};

            let collections = <IntoCollections as FromParser>::from_parser(parser)?;
            let model = Model::new(collections)
                .map_err(EyreReport::msg)
                .wrap_err("Failed to produce a valid model.")?;
            let now = Utc::now();
            let date = now.date();
            let time = now.time();
            let datetime = FixedOffset::east(0)
                .ymd(date.year(), date.month(), date.day())
                .and_hms(time.hour(), time.minute(), time.second());
            ntfs::write(&model, convert_cmd.output.as_path(), datetime)
                .map_err(EyreReport::msg)
                .wrap_err("Failed to write output NTFS format.")?;
        }
    }
    Ok(())
}

#[tracing::instrument(
    level = "error",
    skip(stats_cmd),
    fields(
        from = %stats_cmd.parser_config.from,
    )
)]
fn stats(stats_cmd: &cli::StatsCommand) -> Result<()> {
    use cli::TransitDataFormat;

    tracing::info!("statistics on transit data");

    let model;
    let parser = match stats_cmd.parser_config.from {
        #[cfg(feature = "ntfs")]
        TransitDataFormat::Ntfs => {
            model = transit_model::ntfs::read(stats_cmd.parser_config.input.as_path())
                .map_err(EyreReport::msg)
                .wrap_err("Failed to read input NTFS data.")?;
            fofil_transit_model::Parser::new(&model)
        }
    };
    let stats = fofil_stats::Statistics::from_parser(parser)?;
    for (object_type, count) in stats {
        println!(
            "{}\t{:?}{}",
            count,
            object_type,
            if count > 1 { "s" } else { "" }
        );
    }
    Ok(())
}

fn main() -> Result<()> {
    tracing_subscriber::fmt::init();
    color_eyre::install()?;
    let options = cli::Command::parse();
    match options.subcmd {
        cli::SubCommand::Completion(completion_cmd) => {
            completion(&completion_cmd);
        }
        cli::SubCommand::Convert(convert_cmd) => {
            convert(&convert_cmd)?;
        }
        #[cfg(feature = "stats")]
        cli::SubCommand::Stats(stats_cmd) => {
            stats(&stats_cmd)?;
        }
    }
    Ok(())
}
