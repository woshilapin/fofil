use clap::{Args, Parser, Subcommand, ValueEnum};
use eyre::{eyre, Result};
use std::{path::PathBuf, str::FromStr};

/// Supported transit data format.
#[derive(Debug, ValueEnum, Clone, Copy)]
pub enum TransitDataFormat {
    /// NTFS transit data format (see [NTFS specifications]) is used for
    /// [Navitia] system.
    ///
    /// [Navitia]: https://www.navitia.io/
    /// [NTFS specifications]: https://github.com/CanalTP/ntfs-specification/blob/master/ntfs_fr.md
    #[cfg(feature = "ntfs")]
    Ntfs,
}

impl std::fmt::Display for TransitDataFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TransitDataFormat::Ntfs => write!(f, "ntfs"),
        }
    }
}

impl FromStr for TransitDataFormat {
    type Err = eyre::Report;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            #[cfg(feature = "ntfs")]
            "ntfs" => Ok(TransitDataFormat::Ntfs),
            _ => Err(eyre!("Incorrect format '{}'", s)),
        }
    }
}

#[derive(Debug, ValueEnum, Clone, Copy)]
// ALLOW: Although `PowerShell` does end with the `enum`'s name, it is not an lint error
#[allow(clippy::enum_variant_names)]
pub enum Shell {
    Bash,
    Elvish,
    Fish,
    PowerShell,
    Zsh,
}

impl FromStr for Shell {
    type Err = eyre::Report;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "bash" => Ok(Shell::Bash),
            "elvish" => Ok(Shell::Elvish),
            "fish" => Ok(Shell::Fish),
            "powershell" => Ok(Shell::PowerShell),
            "zsh" => Ok(Shell::Zsh),
            _ => Err(eyre!("Incorrect shell '{}'", s)),
        }
    }
}

#[derive(Debug, Args)]
#[command(version, author)]
pub struct ParserConfig {
    /// Path to the folder for input transit data.
    #[arg(short, long, value_parser)]
    pub input: PathBuf,
    /// Format of the input transit data: ntfs.
    #[arg(short, long, value_enum)]
    pub from: TransitDataFormat,
}

#[derive(Debug, Parser)]
#[command(version, author)]
pub struct Command {
    #[command(subcommand)]
    pub subcmd: SubCommand,
}

#[derive(Debug, Subcommand)]
#[command(version, author)]
pub enum SubCommand {
    /// Provides shell completion scripts for fofil
    #[command(version, author)]
    Completion(CompletionCommand),
    /// Convert transit data from one format to another.
    #[command(version, author)]
    Convert(ConvertCommand),
    #[cfg(feature = "stats")]
    /// Provides statistics about a transit data model.
    #[command(version, author)]
    Stats(StatsCommand),
}

#[derive(Debug, Args)]
#[command(version, author)]
pub struct CompletionCommand {
    #[arg(value_enum)]
    pub shell: Shell,
}

#[derive(Debug, Args)]
#[command(version, author)]
pub struct ConvertCommand {
    #[command(flatten)]
    pub parser_config: ParserConfig,
    /// Path where to output the result of the conversion.
    #[arg(short, long, value_parser)]
    pub output: PathBuf,
    /// Format of the output transit data: ntfs.
    #[arg(short, long, value_enum)]
    pub to: TransitDataFormat,
}

#[cfg(feature = "stats")]
#[derive(Debug, Parser)]
#[command(version, author)]
pub struct StatsCommand {
    #[command(flatten)]
    pub parser_config: ParserConfig,
}
