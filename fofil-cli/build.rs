include!("src/cli.rs");

use clap::CommandFactory;
use clap_complete::{generate_to, shells};

fn main() {
    let mut app = Command::command();
    app.set_bin_name("fofil");

    let outdir = env!("CARGO_MANIFEST_DIR");
    let bash_path = generate_to(shells::Bash, &mut app, "fofil", outdir);
    println!(
        "cargo:info=completion bash file is generated: {:?}",
        bash_path
    );
    let elvish_path = generate_to(shells::Elvish, &mut app, "fofil", outdir);
    println!(
        "cargo:info=completion elvish file is generated: {:?}",
        elvish_path
    );
    let fish_path = generate_to(shells::Fish, &mut app, "fofil", outdir);
    println!(
        "cargo:info=completion fish file is generated: {:?}",
        fish_path
    );
    let powershell_path = generate_to(shells::PowerShell, &mut app, "fofil", outdir);
    println!(
        "cargo:info=completion powershell file is generated: {:?}",
        powershell_path
    );
    let zsh_path = generate_to(shells::Zsh, &mut app, "fofil", outdir);
    println!(
        "cargo:info=completion zsh file is generated: {:?}",
        zsh_path
    );
}
