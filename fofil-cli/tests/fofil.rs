use assert_cmd::prelude::*;
use std::process::Command;
use tempfile::TempDir;

#[test]
fn fofil_convert() {
    let output_dir = TempDir::new().expect("create temp dir failed");
    Command::cargo_bin("fofil")
        .expect("Failed to find binary 'fofil'")
        .arg("convert")
        .arg("--from")
        .arg("ntfs")
        .arg("--input")
        .arg("tests/fixtures/")
        .arg("--to")
        .arg("ntfs")
        .arg("--output")
        .arg(output_dir.path().to_str().unwrap())
        .assert()
        .success();
}
