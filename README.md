[![continuous-integration-badge]][continuous-integration]
[![gitlab-coverage-badge]][gitlab-coverage]
[![codecov-coverage-badge]][codecov-coverage]
[![crates.io-badge]][crates.io]
[![license-badge]][license]
[![documentation-badge]][documentation]

[continuous-integration-badge]: https://img.shields.io/gitlab/pipeline/woshilapin/fofil?style=flat&logo=gitlab&color=fca121
[continuous-integration]: https://gitlab.com/woshilapin/fofil/pipelines
[gitlab-coverage-badge]: https://gitlab.com/woshilapin/fofil/badges/master/coverage.svg
[gitlab-coverage]: https://gitlab.com/woshilapin/fofil/-/commits/master
[codecov-coverage-badge]: https://img.shields.io/codecov/c/gitlab/woshilapin/fofil?style=flat&logo=codecov&color=f01f7a
[codecov-coverage]: https://codecov.io/gl/woshilapin/fofil
[crates.io-badge]: https://img.shields.io/crates/v/fofil?style=flat&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5AUdDAQJPzh3wgAAABBjYU52AAAAKwAAACYAAAAGAAAAA+AjjMkAAAk6SURBVFjDpZdbjF3XWcd/31pr732uc2bOjD3xOMZo4rhKaUOCIvPQCipIG4mLIqoSUSFQiIoQPEERN/HCCyhBvERISNyUShEgWloUEhWcmAdQSXBTV0kcp/gaJ+MZz/XMnDn3vS4fD2fsVEQ4KSxpb21tba3/b631ff/v28L3ODZfegKT1WwYd05o9D8FlCI8n/ZX3iVvpKXPnvme5pMPJfrvv83hH/ljrv/T52edxFOG8DMQP42m7xfjkq20r2j9xItDb57Lem+c7e1uDo+d+nkaJx///wFs/ecf0frh32P7xS98RMu9R9HwKBofEKQmWRXJ5zB5CwVS2QPf7Uma/Iex+VdNZf60NI7fSIMbeuhHn/6/Aayf+WUEqn64/RVV/UljHTafQfIWmILoh1DuQugjRIyxYDLARIWLpPCCGPccWeM1uueH9r7fYuGjn/vwADee/xyZk3p3+/ppQT9Rm78X134A31tBJltIGiECiENsgakfBZOTxlvopAPJA7Inxp1F3NcS9qVqo/1ODJO08GN/CYC7E0AIgRgFjR5NHt9fB3uZuH8Nm1WQrIEYS0oB9UPC3jUkb2KLNnZmHuKQNNmdJfQfIY0ftmLfKYc7L6lr/fXq3z/0avPB3/lgAIAYAmhg3N8k+jEuyxBNkMaoGowpkOoC+AE62SGVHZKtYYo5pLqEMQYNIxuGN5fTZPAr4kdu6bFXX93/r2fvDDAYDgBBYsK5grzaIivqGOswxiKuhinakDVJqoiCUQ+qaBqjo3XSpEMs5nH1JbR7mUlvA5fXprop3BlATRURaCws46zFGhBjMXkLKdqoOKIfoP0VJA4QFBGLuIxEockPJY23kHKXlEbg98kzg7H2tsYdAYwoeZaB3yV4wc7cjWt/nDjpEkeb4PcQ9RgxYBwigsYJYbyLGIMvx6pxInnmSN2LCAljBGMOFqh6ZwDvS4wI+KFKmuCyAjveJnSvYkQxxiGmACIa+qQwQGOJqkJWx1kBubVanR4nEGNCHoO/O/XP/zvAxktPUJs5bHY3rh0fdjZmVCOTwTYxnYdyl6SKLVrkzqChDxoJUVGEzAqCiBjzXZn+XsYrCl85y5X2Zcx3i6698iRngNXTT8zGcvBIf+vSnzkmL4B8zHtPOR4y7r5LORkyHAwZ9XZJYYiQGE6U9d3A2nbJuFREpqsVEUTMbYbpewEKjDHTHVg//Ti2Op/5zrdP3vf8Z3+CSedR1fggIjWxBVGFsvRYmzDGANP01JQhIsQIN3dKQlScFcZeqU8d6kAMMBmaIpCm5kUVFMz2N59k8TPPMO7d/I0Y47+i8SkR+YStzNXszD1IY5moluBLvPe3rxACMQVEYOKVkCAplEGxRkAEmSohrko291Eka+JThpc6zZ+10m7P4srRHuNrXzbDzts/BCxWW0cpFh4k+cE00ss9wngfHwImKcYIYgyCYFxAgJRAFeaaGUmhUXUH2z+9JakQtaqTWAClWC3tn3zqH5nrWdyFC29S3c5pDPYhTRABqS0R9t/BmYSxGSFGyrIkKxogORpGCGBjAIRa1TJTd4BwqOXInaAIUXN8ylXLiOm8gYtDQSNQMGCB2RRxe50dtsMmJ2SCUc+ot01M5w4qXELyGdQ1UdeHfJbK/An6a9+CNMaGCEDuDMcOV6bbIJZSHUoVUolJfdHkGYwDVoRaxYIRKi4jzxwupoQAvvSIloQQ8JN3sc6hGBgFiiwna80QTJ2oQtAM9QNcHm6nWCIjFItoGBPHO9jUIxNPSiU73cBgklBVluYNFSeImcaIm0xKtAz4MAUwxhKNwcQ4jWI7tUtJQ3Idw2CfRqPOaJwRRZikDNQgJqdoHCF1L6FxCKaOWGE4VDb3PCJCkQkhKUaE0nt8iDgfAoSA935qq2aaanIQxUYtYiwaAqNSyFygVrHkNYdkdTQljA7JnCP1rqB+jyyzYKcZUAbFWoOzgo9K7uyBIUw9wX3hqfNm+fhm9elfKF0uHjFTvxaZQlgcxmb0Bp7NrlIrDO2mo1lL5HaE1Qopxen5jzam9eCWB4iSFIwRZuoOa4RKbkCQq1cvy+cfWVQH1GqV2uJat2gfrm1Tr1fJW0cZ714HDagEbBYpy0AI0IuJlJTMKsYljDU4d2D1Rt4zXZnaULuZkRJkbvpcRsvGbtEKa280D937ya4D5Egrn7m2e3h5c3CIuzWwfGQZ7a6RJkMwGTEGhIgPSmYNk1IZjJVKJSKAEUH/Zyk/gChyw9JChVFpeXurYKPbRFzr45/8GAsvn1nrOoBOtzeeTOY6VurHb+zmbLw+iC3TZj4f24YJuBhxJtKsCMYk9ocJXyrxVqDeqjOaUA4cUKan0htn3NipcHUVrrzb4eQ9cxxbasTRzl5a3+lhgep6p+99SG8VmcUZGZ77zupX31qV170szkUabWcSjj65TYxKcAaaVcFljtnZWcxBwZHKIcQIGkt2+hUur9V47arhzUv7bG7uoin1jMtPd7rDPzx34e3X9vYH0U37Ag3nLq69dv7axpXM2UODUVkDamW53MuyY1/cHMy6hfoMR+qbtKo9nI1oUkKIty1XbIHMnGBrbYXLbwu7wybNxiybG28xGY/280rtjMc98/KF1X/78rO/3pOlJ253RH1jJDMiuQ8plj6WQBOY3euPbo7G453ZRmVxFBc5t9roLdR77mhrp9rK9yjitBjFZNjuZbxzfYcrV7usrGxw8kSVQ/NuL6vWXhTNnrnRDd94/NMn+k//7j9wS/x2wC62GzRrBRudvilDzGNM9ZR0FmH+5LH5+x84cddPZ5nLz75149t5ltdP3Xf4UyeP6P33LI7l+PfdzY2tgmurI3Y7e6QYsC7rNFrtf/HJfOmVN1de/s3HHho8/MW/QVN6X+Pzvh+Tk8cOcdfCDK9fXrX9YVmNKbWMkSOqLKpqG6ieuu/uH/jxh+751Xo1yz6yfJRBf8ClS5dwWbYttvj6ONkvnb/eOftzD//g8Bf/4G/v1PW9vyW7tLLFpZUtgAj0gX5Kug40gMPA0f5oMrs/GO40qq275maahNKv57XmCyNvnv3mdza/9eSvfWb41C/9KV//xgU+aNgP/OK9tJ4AHWB1rz+5mJTztSKrGOGN6zc7v3/24tZf3H/v0tU//9or/q+eO/shp4X/Bho9vdLLKtu5AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIwLTA1LTI5VDEwOjA0OjA5KzAyOjAwkCS5BQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMC0wNS0yOVQxMDowNDowOSswMjowMOF5AbkAAAAASUVORK5CYII=&color=e6ac38
[crates.io]: https://crates.io/crates/fofil
[license-badge]: https://img.shields.io/crates/l/fofil?style=flat&logo=spdx&color=4398cc
[license]: https://spdx.org/licenses/MIT.html
[documentation-badge]: https://img.shields.io/badge/docs.rs-fofil-4d76ae?style=flat&logo=data:image/svg+xml;base64,PHN2ZyByb2xlPSJpbWciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDUxMiA1MTIiPjxwYXRoIGZpbGw9IiNmNWY1ZjUiIGQ9Ik00ODguNiAyNTAuMkwzOTIgMjE0VjEwNS41YzAtMTUtOS4zLTI4LjQtMjMuNC0zMy43bC0xMDAtMzcuNWMtOC4xLTMuMS0xNy4xLTMuMS0yNS4zIDBsLTEwMCAzNy41Yy0xNC4xIDUuMy0yMy40IDE4LjctMjMuNCAzMy43VjIxNGwtOTYuNiAzNi4yQzkuMyAyNTUuNSAwIDI2OC45IDAgMjgzLjlWMzk0YzAgMTMuNiA3LjcgMjYuMSAxOS45IDMyLjJsMTAwIDUwYzEwLjEgNS4xIDIyLjEgNS4xIDMyLjIgMGwxMDMuOS01MiAxMDMuOSA1MmMxMC4xIDUuMSAyMi4xIDUuMSAzMi4yIDBsMTAwLTUwYzEyLjItNi4xIDE5LjktMTguNiAxOS45LTMyLjJWMjgzLjljMC0xNS05LjMtMjguNC0yMy40LTMzLjd6TTM1OCAyMTQuOGwtODUgMzEuOXYtNjguMmw4NS0zN3Y3My4zek0xNTQgMTA0LjFsMTAyLTM4LjIgMTAyIDM4LjJ2LjZsLTEwMiA0MS40LTEwMi00MS40di0uNnptODQgMjkxLjFsLTg1IDQyLjV2LTc5LjFsODUtMzguOHY3NS40em0wLTExMmwtMTAyIDQxLjQtMTAyLTQxLjR2LS42bDEwMi0zOC4yIDEwMiAzOC4ydi42em0yNDAgMTEybC04NSA0Mi41di03OS4xbDg1LTM4Ljh2NzUuNHptMC0xMTJsLTEwMiA0MS40LTEwMi00MS40di0uNmwxMDItMzguMiAxMDIgMzguMnYuNnoiPjwvcGF0aD48L3N2Zz4K&color=4d76ae
[documentation]: https://woshilapin.gitlab.io/fofil/

Fofil
=====

Fofil is a public transportation data convertor. It aims at supporting [GTFS]
and [NTFS] at the very least.

At the moment, it's a very experimental, in-progress project.

# Why the name Fofil?
It comes from the French __se faufiler__ which means getting through the crowd,
always finding a path (literal translation would be __weave in and out__). It
felt like taking public transport is always an adventure where you need to know
how to get through the multiple transportation options, the crowdy city with
people, bikes, cars, skateboards and who knows what. Therefore, one would
__faufile__ ([IPA] spelling is __fofile__) itself to reach destination. The
spelling has been simplified to be easier to pronounce in English.

# Goals of the project
Most of the thinking come from flaws in the design of [`transit_model`] which is
a great tool to convert public transport data but whose architecture is not
really organized and of which I believe performances could be improved
(processing as well as memory).

Below are some axis of reflexion.

# Architecture
## Visitor Pattern
The [Visitor Pattern] seems particularly adapted for exporting the model into
different formats (GTFS, NTFS, NeTEx, etc.).

## `serde` architecture
The `serde` architecture looks very close to what we need (from string to model,
then from model to string). The difference here is what the model is:
- in `serde`, the model is some primitive Rust elements (integers, structure,
  sequences, etc.)
- our model is about public transportation (networks, lines, routes, stops)

## Testing
How about using something like `spectral` as an assertion library?

## Memory
### Identifiers
Do not store a String every time we need an identifier, only the object owning
the identifier should store it, the others should refer to it:
- either a reference to the identifier (likely a `&String`)
- or better, store the referenced object directly if the model is some sort of
  tree

### `CollectionWithId<T>`
`CollectionWithId<T>` is not that efficient in memory since it stores twice
every `Id` (in the `Vec<T>` and in the `HashMap<String, Idx<T>>`).

`CollectionWithId<T>` uses `Idx<T>` which are essentially offsets (but typed to
the type of objects in the collection). Some bugs were encountered due to the
fact that some offset were not updated correctly (especially in `StopTime` which
uses `Idx<T>` to store `String` outside of the `StopTime` object due to the high
numbers of them).

#### Using UUID instead of offset
If we were using a UUID instead of keeping offsets, we wouldn’t need to update
offsets when we remove some object in the array since their index would still be
the same UUID (when using offsets, removing an object need an update of all
offset references).

`CollectionWithId<T>` is today `(Vec<T>, HashMap<String, Idx<T>>)` but it could
be `(HashMap<Uuid, T>, HashMap<String, Uuid>)`. Note that this solution doesn't
solve the memory problem of storing twice the identifier. To be precise, it
actually make it worse since a `Idx<T>` is about 32 bits whereas a UUID would be
128 bits.

#### Replacing `CollectionWithId<T>` with `HashSet<T>`
The main goal is to be able to store objects of the same type and access them by
their identifier in minimum time. A `Vec<T>` would be the most efficient in term
of memory but it would need `O(n)` complexity to find an object in it. A
`HashMap<T>` would `O(1)` to look up an object by identifier but the identifier
would be stored twice (as the key and in the value).

An option would be to directly use a `HashSet<T>`. The memory would be similar
to `Vec<T>` and it seems possible to access in `O(1)` (see below).

The lookup method `HashSet::<T>::get()` doesn't take a `&T` as a parameter but
any kind of object `&Q` that respects `T: Borrow<Q>`. This means that if our
type `T` implements `Borrow<Q>`, then we can query by `Q` in the `HashSet<T>`.
In our case, `Q` would be `String`, the implementation for `T` would just return
a pointer to the identifier of the object.

This is great but `HashSet::<T>::get(&self, value: &Q)` will actually hash
`value` in order to operate the lookup. If our object `T` has been hashed from
all its fields, then the 2 hashes would be different and we would never find the
object. Therefore, we also need to implement correctly the `Hash` trait for `T`,
using only the identifier for hashing. Another advantage is that the hashing of
object would be faster. This is a reasonable thing to do since identifiers are
supposed to be unique.

Note: it would be nice to also implement `Borrow<str>` so look up in the
`HashSet` can be done both by `&String` and `&str`.

# Unanswered questions
- Why Stop Points must be inside a Stop Area? If the Stop Point is alone, in term of modelling, the Stop Area has no meaning since it doesn’t regroup multiple Stop Points
- Must the `trip_id` be unique over all the `trip_id` of the model or only among the Trips of a Route?

[GTFS]: https://developers.google.com/transit/gtfs/reference/
[IPA]: https://www.wikiwand.com/en/International_Phonetic_Alphabet
[NTFS]: https://github.com/CanalTP/ntfs-specification/
[`transit_model`]: https://github.com/CanalTP/transit_model
[Visitor Pattern]: https://refactoring.guru/design-patterns/visitor
