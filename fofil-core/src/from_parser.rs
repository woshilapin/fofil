use crate::Parser;

/// A trait to build an object from a [`Parser`].
pub trait FromParser: Sized {
    /// Value produced as the output from parsing.
    type Value;
    /// Function taking a [`Parser`] and producing a `Self`.
    fn from_parser<P>(parser: P) -> Result<Self::Value, P::Error>
    where
        P: Parser;
}
