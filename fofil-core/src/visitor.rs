use crate::{
    view::{
        CalendarView, EquipmentView, JourneyPatternView, JourneyStopView, LevelView, LineView,
        NetworkView, RouteView, ScheduledJourneyView, ScheduledStopView, StopAreaView,
        StopPointView,
    },
    Error,
};

/// Trait to visit all the elements of the transit data model.
pub trait Visitor: Sized {
    /// Type of value produced by the visitor.
    type Value;

    /// Visit the calendar.
    ///
    /// Default implementation will panic.
    fn visit_calendar<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: CalendarView<'model>,
    {
        tracing::debug!("default implementation for visit_calendar");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the equipment.
    ///
    /// Default implementation will panic.
    fn visit_equipment<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: EquipmentView<'model>,
    {
        tracing::debug!("default implementation for visit_equipment");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the journey pattern.
    ///
    /// Default implementation will panic.
    fn visit_journey_pattern<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: JourneyPatternView<'model>,
    {
        tracing::debug!("default implementation for visit_journey_pattern");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the journey stop.
    ///
    /// Default implementation will panic.
    fn visit_journey_stop<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: JourneyStopView<'model>,
    {
        tracing::debug!("default implementation for visit_journey_stop");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the level/floor.
    ///
    /// Default implementation will panic.
    fn visit_level<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: LevelView<'model>,
    {
        tracing::debug!("default implementation for visit_level");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the line.
    ///
    /// Default implementation will panic.
    fn visit_line<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: LineView<'model>,
    {
        tracing::debug!("default implementation for visit_line");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the network.
    ///
    /// Default implementation will panic.
    fn visit_network<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: NetworkView<'model>,
    {
        tracing::debug!("default implementation for visit_network");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the route.
    ///
    /// Default implementation will panic.
    fn visit_route<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: RouteView<'model>,
    {
        tracing::debug!("default implementation for visit_route");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the scheduled journey.
    ///
    /// Default implementation will panic.
    fn visit_scheduled_journey<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: ScheduledJourneyView<'model>,
    {
        tracing::debug!("default implementation for visit_scheduled_journey");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the scheduled stop.
    ///
    /// Default implementation will panic.
    fn visit_scheduled_stop<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: ScheduledStopView<'model>,
    {
        tracing::debug!("default implementation for visit_scheduled_stop");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the stop area.
    ///
    /// Default implementation will panic.
    fn visit_stop_area<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: StopAreaView<'model>,
    {
        tracing::debug!("default implementation for visit_stop_area");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }

    /// Visit the stop point.
    ///
    /// Default implementation will panic.
    fn visit_stop_point<'model, V>(self, _view: V) -> Result<Self::Value, V::Error>
    where
        V: StopPointView<'model>,
    {
        tracing::debug!("default implementation for visit_stop_point");
        Err(V::Error::invalid_type(
            std::any::type_name::<Self::Value>().to_string(),
            "None".to_string(),
        ))
    }
}
