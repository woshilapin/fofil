use thiserror::Error;

/// Type of element in a transit data model.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum ObjectType {
    /// Calendar of a transit data model.
    Calendar,
    /// Equipment of a transit data model.
    Equipment,
    /// Journey pattern of a transit data model.
    JourneyPattern,
    /// Journey stop of a transit data model.
    JourneyStop,
    /// Level/Floor of a transit data model.
    Level,
    /// Line of a transit data model.
    Line,
    /// Network of a transit data model.
    Network,
    /// Route of a transit data model.
    Route,
    /// Scheduled journey/vehicle journey/trip of a transit data model.
    ScheduledJourney,
    /// Scheduled stop/stop time of a transit data model.
    ScheduledStop,
    /// Stop area of a transit data model.
    StopArea,
    /// Stop point of a transit data model.
    StopPoint,
}

/// Trait abstracting errors in transit data model parsing.
pub trait Error: std::error::Error + Sized + Send + Sync + 'static {
    /// Used when an identifier is not found during parsing.
    fn identifier_not_found(object_type: ObjectType, identifier: String) -> Self;
    /// Used when an identifier is found multiple times.
    fn identifier_collision(object_type: ObjectType, identifier: String) -> Self;
    /// Used when a value cannot be forced into a specific type.
    ///
    /// To provide the  `type_name`, `std::any::type_name::<T>()` can be used.
    fn invalid_type(type_name: String, value: String) -> Self;
    /// Emitted when a property does not exist in source data.
    fn no_such_property(object_type: ObjectType, identifier: String, property_name: String)
        -> Self;
}

/// Provide a default implementation of the [`Error`] trait.
#[derive(Debug, Error, PartialEq, Eq)]
pub enum FofilError {
    /// Emitted when an identifier is not found during parsing.
    ///
    /// Contains the type of object and the identifier that is not found.
    #[error("{0:?} with identifier '{1}' not found")]
    IdentifierNotFound(ObjectType, String),
    /// Emitted when an identifier is found multiple times.
    ///
    /// Contains the type of object and the identifier that is found.
    #[error("Multiple {0:?} found with the same identifier '{1}'")]
    IdentifierCollision(ObjectType, String),
    /// Emitted when a value cannot be forced into a specific type.
    #[error("Unable to provide a type '{0}' from '{1}'")]
    InvalidType(String, String),
    /// Emitted when a property does not exist in source data.
    #[error("The property '{2}' does not exist in type '{0:?}' with identifier '{1}'")]
    NoSuchProperty(ObjectType, String, String),
}

impl Error for FofilError {
    fn identifier_not_found(object_type: ObjectType, identifier: String) -> Self {
        Self::IdentifierNotFound(object_type, identifier)
    }
    fn identifier_collision(object_type: ObjectType, identifier: String) -> Self {
        Self::IdentifierCollision(object_type, identifier)
    }
    fn invalid_type(type_name: String, value: String) -> Self {
        Self::InvalidType(type_name, value)
    }
    fn no_such_property(
        object_type: ObjectType,
        identifier: String,
        property_name: String,
    ) -> Self {
        Self::NoSuchProperty(object_type, identifier, property_name)
    }
}
