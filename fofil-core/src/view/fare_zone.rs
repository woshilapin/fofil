use crate::Error;

/// Provide a view of the fare zone.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed fare zone (used for pricing) and give access to
/// its properties.
pub trait FareZoneView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Get the identifier of the fare zone.
    fn id(&self) -> Result<String, Self::Error>;
}
