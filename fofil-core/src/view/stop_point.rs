use crate::{
    view::{CoordinateView, EquipmentView, FareZoneView, LevelView, StopAreaView},
    Error,
};
use chrono_tz::Tz;

/// Provide a view of a stop point.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed stop point and give access to all its properties.
pub trait StopPointView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the coordinates of the stop point.
    type CoordinateView: CoordinateView<'model, Error = Self::Error>;
    /// Associated type providing a view on the available equipments of the stop
    /// point.
    type EquipmentView: EquipmentView<'model, Error = Self::Error>;
    /// Associated type providing a view on the fare zone (for pricing) of the
    /// stop point.
    type FareZoneView: FareZoneView<'model, Error = Self::Error>;
    /// Associated type providing a view on the level/floor of the stop point.
    type LevelView: LevelView<'model, Error = Self::Error>;
    /// Associated type providing a view on the parent stop area of the stop
    /// point.
    type StopAreaView: StopAreaView<'model, Error = Self::Error>;

    /// Get the identifier of the stop point.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the name of the stop point.
    fn name(&self) -> Result<String, Self::Error>;
    /// Get a view on the parent stop area of the stop point.
    fn stop_area(&self) -> Result<Self::StopAreaView, Self::Error>;
    /// Get the visibility of the stop point (for example, can be used for
    /// availability in autocompletion systems).
    fn visible(&self) -> Result<bool, Self::Error>;
    /// Get a view on the geolocated coordinates of the stop point.
    fn coordinates(&self) -> Result<Self::CoordinateView, Self::Error>;
    /// Get a view on the fare zone where the stop point is located.
    fn fare_zone(&self) -> Result<Option<Self::FareZoneView>, Self::Error>;
    /// Get the timezone in which the stop point is located.
    fn timezone(&self) -> Result<Option<Tz>, Self::Error>;
    /// Get a view on the equipments available at the stop point.
    fn equipment(&self) -> Result<Option<Self::EquipmentView>, Self::Error>;
    /// Get a view on the level/floor where the stop point is located.
    fn level(&self) -> Result<Option<Self::LevelView>, Self::Error>;
    /// Get the code of the platform of the stop point.
    fn platform_code(&self) -> Result<Option<String>, Self::Error>;
}
