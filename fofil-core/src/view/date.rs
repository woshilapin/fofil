use crate::Error;

/// Provide a view of a date.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the dates and give access to all its properties.
pub trait DateView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Get the year of the date.
    fn year(&self) -> Result<isize, Self::Error>;
    /// Get the month of the date.
    fn month(&self) -> Result<usize, Self::Error>;
    /// Get the day of the date.
    fn day(&self) -> Result<usize, Self::Error>;
}

#[cfg(feature = "chrono")]
impl<'model> DateView<'model> for chrono::NaiveDate {
    type Error = crate::FofilError;
    fn year(&self) -> Result<isize, Self::Error> {
        let year = chrono::Datelike::year(self);
        std::convert::TryFrom::try_from(year).map_err(|_| {
            Self::Error::invalid_type(std::any::type_name::<i32>().to_string(), year.to_string())
        })
    }
    fn month(&self) -> Result<usize, Self::Error> {
        let month = chrono::Datelike::month(self);
        std::convert::TryFrom::try_from(month).map_err(|_| {
            Self::Error::invalid_type(std::any::type_name::<u32>().to_string(), month.to_string())
        })
    }
    fn day(&self) -> Result<usize, Self::Error> {
        let day = chrono::Datelike::day(self);
        std::convert::TryFrom::try_from(day).map_err(|_| {
            Self::Error::invalid_type(std::any::type_name::<u32>().to_string(), day.to_string())
        })
    }
}
