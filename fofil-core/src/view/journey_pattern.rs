use crate::{
    view::{JourneyStopView, LineView, RouteView, ScheduledJourneyView},
    Error,
};

// These type aliases are just a trick to make the return types of the method
// below a little bit more readable. Ideally, the return type should be `impl
// SomeView` but this syntax is not yet possible inside `traits` [RFC1522].
//
// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
type ParentRouteView<'model, T> = <T as JourneyPatternView<'model>>::RouteView;
type ParentLineView<'model, T> = <ParentRouteView<'model, T> as RouteView<'model>>::LineView;
type ParentNetworkView<'model, T> = <ParentLineView<'model, T> as LineView<'model>>::NetworkView;

/// Provide a view of a journey pattern.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed journey pattern and give access to all its properties.
pub trait JourneyPatternView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the parent route.
    type RouteView: RouteView<'model, Error = Self::Error>;
    /// Associated type providing a view on the scheduled journey inside the
    /// journey pattern.
    type ScheduledJourneyView: ScheduledJourneyView<'model, Error = Self::Error>;
    /// Iterator type yielding
    /// [`ScheduledJourneyView`](JourneyPatternView::ScheduledJourneyView)
    /// views.
    type ScheduledJourneyViewIter: Iterator<Item = Self::ScheduledJourneyView>;
    /// Associated type providing a view on the journey stops inside the journey
    /// pattern.
    type JourneyStopView: JourneyStopView<'model, Error = Self::Error>;
    /// Iterator type yielding
    /// [`JourneyStopView`](JourneyPatternView::JourneyStopView) views.
    type JourneyStopViewIter: Iterator<Item = Self::JourneyStopView>;
    /// Get the identifier of the journey pattern.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the parent route view [`RouteView`].
    fn route(&self) -> Result<Self::RouteView, Self::Error>;
    /// Get the parent line view [`LineView`].
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::LineView;
    /// # trait Line<'model> {
    /// # type Error;
    /// fn line(&self) -> Result<impl LineView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn line(&self) -> Result<ParentLineView<'model, Self>, Self::Error> {
        self.route()?.line()
    }
    /// Get the parent network view [`NetworkView`](crate::view::NetworkView).
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::NetworkView;
    /// # trait Network<'model> {
    /// # type Error;
    /// fn network(&self) -> Result<impl NetworkView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn network(&self) -> Result<ParentNetworkView<'model, Self>, Self::Error> {
        self.line()?.network()
    }
    /// Get views on all the scheduled journey inside the journey pattern.
    fn scheduled_journeys(&self) -> Result<Self::ScheduledJourneyViewIter, Self::Error>;
    /// Get views on all the journey stops inside the journey pattern.
    fn journey_stops(&self) -> Result<Self::JourneyStopViewIter, Self::Error>;
}
