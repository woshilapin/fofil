use crate::Error;
use rust_decimal::Decimal;

/// Provide a view of a level/floor.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed level/floor and give access to all its properties.
pub trait LevelView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Get the identifier of the level.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the elevation of the level.
    fn floor(&self) -> Result<Decimal, Self::Error>;
    /// Get the name of the level.
    fn name(&self) -> Result<Option<String>, Self::Error>;
}
