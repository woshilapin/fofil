use crate::{
    view::{JourneyPatternView, LineView},
    Error,
};

// These type aliases are just a trick to make the return types of the method
// below a little bit more readable. Ideally, the return type should be `impl
// SomeView` but this syntax is not yet possible inside `traits` [RFC1522].
//
// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
type ParentLineView<'model, T> = <T as RouteView<'model>>::LineView;
type ParentNetworkView<'model, T> = <ParentLineView<'model, T> as LineView<'model>>::NetworkView;

/// Provide a view of a line.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed line and give access to all its properties.
pub trait RouteView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the parent line.
    type LineView: LineView<'model, Error = Self::Error>;
    /// Associated type providing a view on the journey patterns inside the
    /// route.
    type JourneyPatternView: JourneyPatternView<'model, Error = Self::Error>;
    /// Iterator type yielding
    /// [`JourneyPatternView`](RouteView::JourneyPatternView) views.
    type JourneyPatternViewIter: Iterator<Item = Self::JourneyPatternView>;
    /// Get the identifier of the line.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the name of the line.
    fn name(&self) -> Result<String, Self::Error>;
    /// Get the parent line view [`LineView`].
    fn line(&self) -> Result<Self::LineView, Self::Error>;
    /// Get the parent network view [`NetworkView`](crate::view::NetworkView).
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::NetworkView;
    /// # trait Network<'model> {
    /// # type Error;
    /// fn network(&self) -> Result<impl NetworkView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn network(&self) -> Result<ParentNetworkView<'model, Self>, Self::Error> {
        self.line()?.network()
    }
    /// Get views on all the journey patterns inside the route.
    fn journey_patterns(&self) -> Result<Self::JourneyPatternViewIter, Self::Error>;
}
