use crate::{view::DateView, Error};

/// Provide a view of an calendar.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed calendar and give access to all its properties.
pub trait CalendarView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the dates.
    type DateView: DateView<'model, Error = Self::Error>;
    /// Iterator type yielding [`DateView`](CalendarView::DateView) views.
    type DateViewIter: Iterator<Item = Self::DateView>;
    /// Get the identifier of the equipment.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get views on all the dates inside the calendar.
    fn dates(&self) -> Result<Self::DateViewIter, Self::Error>;
}
