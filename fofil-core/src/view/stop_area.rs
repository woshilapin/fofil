use crate::{view::StopPointView, Error};

/// Provide a view of a stop area.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed stop area and give access to all its properties.
pub trait StopAreaView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the stop points inside the stop
    /// area.
    type StopPointView: StopPointView<'model, Error = Self::Error>;
    /// Get the identifier of the stop area.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the name of the stop area.
    fn name(&self) -> Result<String, Self::Error>;
    /// Get views on all the stop points inside the stop area.
    fn stop_points(&self) -> Result<Vec<Self::StopPointView>, Self::Error>;
}
