use crate::{view::LineView, Error};

/// Provide a view of a network.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed network and give access to all its properties.
pub trait NetworkView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the lines inside the network.
    type LineView: LineView<'model, Error = Self::Error>;
    /// Iterator type yielding [`LineView`](NetworkView::LineView) views.
    type LineViewIter: Iterator<Item = Self::LineView>;
    /// Get the identifier of the network.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the name of the network.
    fn name(&self) -> Result<String, Self::Error>;
    /// Get views on all the lines inside the network.
    fn lines(&self) -> Result<Self::LineViewIter, Self::Error>;
}
