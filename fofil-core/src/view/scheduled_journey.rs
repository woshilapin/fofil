use crate::{
    view::{CalendarView, JourneyPatternView, LineView, RouteView, ScheduledStopView},
    Error,
};

// These type aliases are just a trick to make the return types of the method
// below a little bit more readable. Ideally, the return type should be `impl
// SomeView` but this syntax is not yet possible inside `traits` [RFC1522].
//
// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
type ParentJourneyPatternView<'model, T> = <T as ScheduledJourneyView<'model>>::JourneyPatternView;
type ParentRouteView<'model, T> =
    <ParentJourneyPatternView<'model, T> as JourneyPatternView<'model>>::RouteView;
type ParentLineView<'model, T> = <ParentRouteView<'model, T> as RouteView<'model>>::LineView;
type ParentNetworkView<'model, T> = <ParentLineView<'model, T> as LineView<'model>>::NetworkView;

/// Provide a view of a scheduled journey.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed scheduled journey and give access to all its properties.
pub trait ScheduledJourneyView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the parent journey pattern.
    type JourneyPatternView: JourneyPatternView<'model, Error = Self::Error>;
    /// Associated type providing a view on the calendar of the scheduled
    /// journey/vehicle journey/trip.
    type CalendarView: CalendarView<'model, Error = Self::Error>;
    /// Associated type providing a view on the scheduled stops/stop times
    /// inside the scheduled journey/vehicle journey/trip.
    type ScheduledStopView: ScheduledStopView<'model, Error = Self::Error>;
    /// Iterator type yielding
    /// [`ScheduledStopView`](ScheduledJourneyView::ScheduledStopView) views.
    type ScheduledStopViewIter: Iterator<Item = Self::ScheduledStopView>;
    /// Get the identifier of the scheduled journey.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the parent journey pattern view [`JourneyPatternView`].
    fn journey_pattern(&self) -> Result<Self::JourneyPatternView, Self::Error>;
    /// Get the calendar view [`CalendarView`]
    fn calendar(&self) -> Result<Self::CalendarView, Self::Error>;
    /// Get the parent route view [`RouteView`].
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::RouteView;
    /// # trait Route<'model> {
    /// # type Error;
    /// fn route(&self) -> Result<impl RouteView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn route(&self) -> Result<ParentRouteView<'model, Self>, Self::Error> {
        self.journey_pattern()?.route()
    }
    /// Get the parent line view [`LineView`].
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::LineView;
    /// # trait Line<'model> {
    /// # type Error;
    /// fn line(&self) -> Result<impl LineView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn line(&self) -> Result<ParentLineView<'model, Self>, Self::Error> {
        self.route()?.line()
    }
    /// Get the parent network view [`NetworkView`](crate::view::NetworkView).
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::NetworkView;
    /// # trait Network<'model> {
    /// # type Error;
    /// fn network(&self) -> Result<impl NetworkView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn network(&self) -> Result<ParentNetworkView<'model, Self>, Self::Error> {
        self.line()?.network()
    }
    /// Get all the scheduled stops/stop times views
    /// [`ScheduledStopView`] inside the scheduled journey/vehicle
    /// journey/trip.
    fn scheduled_stops(&self) -> Result<Self::ScheduledStopViewIter, Self::Error>;
}
