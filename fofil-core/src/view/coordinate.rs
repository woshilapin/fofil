use crate::Error;

/// Provide a view of geolocated coordinates.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed geolocated coordinates and give access to latitude and
/// longitude.
pub trait CoordinateView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Get the latitude.
    fn latitude(&self) -> Result<f64, Self::Error>;
    /// Get the longitude.
    fn longitude(&self) -> Result<f64, Self::Error>;
}
