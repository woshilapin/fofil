use crate::{
    view::{NetworkView, RouteView},
    Error,
};

/// Provide a view of a line.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed line and give access to all its properties.
pub trait LineView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the parent network.
    type NetworkView: NetworkView<'model, Error = Self::Error>;
    /// Associated type providing a view on the route inside the line.
    type RouteView: RouteView<'model, Error = Self::Error>;
    /// Iterator type yielding [`RouteView`](LineView::RouteView) views.
    type RouteViewIter: Iterator<Item = Self::RouteView>;
    /// Get the identifier of the line.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the name of the line.
    fn name(&self) -> Result<String, Self::Error>;
    /// Get the parent network view [`NetworkView`].
    fn network(&self) -> Result<Self::NetworkView, Self::Error>;
    /// Get views on all the routes inside the line.
    fn routes(&self) -> Result<Self::RouteViewIter, Self::Error>;
}
