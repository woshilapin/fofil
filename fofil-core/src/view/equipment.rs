use crate::Error;

/// Provide a view of an equipment.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed equipment and give access to all its properties.
pub trait EquipmentView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Get the identifier of the equipment.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the wheelchair accessibility.
    fn wheelchair_boarding(&self) -> Result<Option<bool>, Self::Error>;
}
