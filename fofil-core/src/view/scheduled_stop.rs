use crate::{
    view::{JourneyPatternView, JourneyStopView, LineView, RouteView, ScheduledJourneyView},
    Error,
};

// These type aliases are just a trick to make the return types of the method
// below a little bit more readable. Ideally, the return type should be `impl
// SomeView` but this syntax is not yet possible inside `traits` [RFC1522].
//
// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
type ParentScheduledJourneyView<'model, T> = <T as ScheduledStopView<'model>>::ScheduledJourneyView;
type ParentJourneyPatternView<'model, T> =
    <ParentScheduledJourneyView<'model, T> as ScheduledJourneyView<'model>>::JourneyPatternView;
type ParentRouteView<'model, T> =
    <ParentJourneyPatternView<'model, T> as JourneyPatternView<'model>>::RouteView;
type ParentLineView<'model, T> = <ParentRouteView<'model, T> as RouteView<'model>>::LineView;
type ParentNetworkView<'model, T> = <ParentLineView<'model, T> as LineView<'model>>::NetworkView;

/// Provide a view of a scheduled stop.
///
/// When browsing a transit data model, implementors of this trait will provide
/// a view of the browsed scheduled stop and give access to all its properties.
pub trait ScheduledStopView<'model>: Sized {
    /// Error type returned by this trait implementor.
    type Error: Error;
    /// Associated type providing a view on the parent scheduled journey/vehicle
    /// journey/trip.
    type ScheduledJourneyView: ScheduledJourneyView<'model, Error = Self::Error>;
    /// Associated type providing a view on the parent journey stop.
    type JourneyStopView: JourneyStopView<'model, Error = Self::Error>;
    /// Get the identifier of the scheduled stop.
    fn id(&self) -> Result<String, Self::Error>;
    /// Get the parent scheduled journey view
    /// [`ScheduledJourneyView`].
    fn scheduled_journey(&self) -> Result<Self::ScheduledJourneyView, Self::Error>;
    /// Get the parent journey stop view [`JourneyStopView`].
    fn journey_stop(&self) -> Result<Self::JourneyStopView, Self::Error>;
    /// Get the parent journey pattern view [`JourneyPatternView`].
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::JourneyPatternView;
    /// # trait JourneyPattern<'model> {
    /// # type Error;
    /// fn journey_pattern(&self) -> Result<impl JourneyPatternView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    // An alternative implementation is also possible, using the graph of
    // relations through the scheduled stop but it is believed that in general,
    // the volumetry of scheduled journeys is less important than scheduled
    // stops by an order of magnitude and therefore, should be more efficient.
    // ```
    // fn journey_pattern(
    //     &self,
    // ) -> Result<
    //     <Self::JourneyStopView as JourneyStopView<'model>>::JourneyPatternView,
    //     Self::Error,
    // > {
    //     self.journey_stop()?.journey_pattern()
    // }
    // ```
    fn journey_pattern(&self) -> Result<ParentJourneyPatternView<'model, Self>, Self::Error> {
        self.scheduled_journey()?.journey_pattern()
    }
    /// Get the parent route view [`RouteView`].
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::RouteView;
    /// # trait Route<'model> {
    /// # type Error;
    /// fn route(&self) -> Result<impl RouteView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn route(&self) -> Result<ParentRouteView<'model, Self>, Self::Error> {
        self.journey_pattern()?.route()
    }
    /// Get the parent line view [`LineView`].
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::LineView;
    /// # trait Line<'model> {
    /// # type Error;
    /// fn line(&self) -> Result<impl LineView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn line(&self) -> Result<ParentLineView<'model, Self>, Self::Error> {
        self.route()?.line()
    }
    /// Get the parent network view [`NetworkView`](crate::view::NetworkView).
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::view::NetworkView;
    /// # trait Network<'model> {
    /// # type Error;
    /// fn network(&self) -> Result<impl NetworkView<'model>, Self::Error> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn network(&self) -> Result<ParentNetworkView<'model, Self>, Self::Error> {
        self.line()?.network()
    }
}
