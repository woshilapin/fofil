#![warn(
    missing_docs,
    rustdoc::missing_doc_code_examples,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    rust_2018_idioms,
    rustdoc::broken_intra_doc_links
)]
//! Fofil Core provides the framework for parsing and converting transit data
//! model.
//!
//! # Architecture
//! Fofil framework is greatly inspired by how [`serde`] is designed. [`serde`]
//! provides a way to convert from/to a data format into a Rust structure by
//! going trough an Intermediate Representation (IR): the [`serde`] data model.
//!
//! With a similar design, Fofil defines its own transit data model which serves
//! as the IR. One can convert any kind of transit data model into the Fofil IR
//! by implementing the [`Parser`] trait. Another can transform Fofil IR into
//! anything by implementing the [`FromParser`] trait.
//!
//! [`FromParser`]: ./trait.FromParser.html
//! [`Parser`]: ./trait.Parser.html
//! [`serde`]: https://serde.rs/

mod error;
mod from_parser;
mod parser;
mod visitor;

pub use error::{Error, FofilError, ObjectType};
pub use from_parser::FromParser;
pub use parser::{Parser, VisitorFactory};
pub use visitor::Visitor;

pub mod view;
