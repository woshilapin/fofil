use crate::{Error, Visitor};
use dyn_iter::DynIter;
use std::marker::PhantomData;

pub trait IteratorFunction<P, V>: FnMut(&mut P, V) -> Option<Result<V::Value, P::Error>>
where
    P: Parser,
    V: Visitor,
{
}
impl<T, P, V> IteratorFunction<P, V> for T
where
    P: Parser,
    V: Visitor,
    T: FnMut(&mut P, V) -> Option<Result<V::Value, P::Error>>,
{
}

/// Trait for functions that are able to build an instance of [`Visitor`].  Any
/// function/closure that implements `Fn() -> V` where `V` is an instance of
/// [`Visitor`] automatically implements [`VisitorFactory`].
pub trait VisitorFactory<V>: Fn() -> V
where
    V: Visitor,
{
}
impl<T, V> VisitorFactory<V> for T
where
    V: Visitor,
    T: Fn() -> V,
{
}

struct ParserIter<'parser, P, V, VF, F>
where
    P: Parser,
    V: Visitor,
    VF: VisitorFactory<V>,
    F: IteratorFunction<P, V>,
{
    parser: &'parser mut P,
    visitor_factory: VF,
    next: F,
    _marker: PhantomData<V>,
}

impl<'parser, P, V, VF, F> ParserIter<'parser, P, V, VF, F>
where
    P: Parser,
    V: Visitor,
    VF: VisitorFactory<V>,
    F: IteratorFunction<P, V>,
{
    fn new(parser: &'parser mut P, visitor_factory: VF, next: F) -> Self {
        Self {
            parser,
            visitor_factory,
            next,
            _marker: PhantomData::default(),
        }
    }
}

impl<'parser, P, V, VF, F> Iterator for ParserIter<'parser, P, V, VF, F>
where
    P: Parser,
    V: Visitor,
    VF: VisitorFactory<V>,
    F: IteratorFunction<P, V>,
{
    type Item = Result<V::Value, P::Error>;
    fn next(&mut self) -> Option<Self::Item> {
        let visitor_factory = &mut self.visitor_factory;
        let visitor = visitor_factory();
        let next = &mut self.next;
        next(self.parser, visitor)
    }
}

/// Parser for a transit data model.
pub trait Parser: Sized {
    /// Error type returned when parsing fails.
    type Error: Error;

    /// Parse a calendar and receive a [`Visitor`] that will be driven.
    fn next_calendar<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse calendars as an iterator and receive a [`VisitorFactory`] that
    /// will be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait Calendars<'model, V: Visitor> {
    /// # type Error;
    /// fn calendars(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn calendars<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(self, visitor_factory, Self::next_calendar))
    }

    /// Parse an equipment and receive a [`Visitor`] that will be driven.
    fn next_equipment<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse equipments as an iterator and receive a [`VisitorFactory`] that
    /// will be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait Equipments<'model, V: Visitor> {
    /// # type Error;
    /// fn equipments(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn equipments<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(self, visitor_factory, Self::next_equipment))
    }

    /// Parse a journey pattern and receive a [`Visitor`] that will be driven.
    fn next_journey_pattern<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse journey patterns as an iterator and receive a [`VisitorFactory`]
    /// that will be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait JourneyPattern<'model, V: Visitor> {
    /// # type Error;
    /// fn journey_patterns(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn journey_patterns<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(
            self,
            visitor_factory,
            Self::next_journey_pattern,
        ))
    }

    /// Parse a journey pattern stop and receive a [`Visitor`] that will be
    /// driven.
    fn next_journey_stop<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse journey pattern stops as an iterator and receive a
    /// [`VisitorFactory`] that will be used to produce [`Visitor`] for each
    /// iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait JourneyStop<'model, V: Visitor> {
    /// # type Error;
    /// fn journey_stops(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn journey_stops<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(
            self,
            visitor_factory,
            Self::next_journey_stop,
        ))
    }

    /// Parse a level/floor and receive a [`Visitor`] that will be driven.
    fn next_level<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse levels as an iterator and receive a [`VisitorFactory`] that will
    /// be
    /// used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait Level<'model, V: Visitor> {
    /// # type Error;
    /// fn levels(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn levels<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(self, visitor_factory, Self::next_level))
    }

    /// Parse a line and receive a [`Visitor`] that will be driven.
    fn next_line<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse lines as an iterator and receive a [`VisitorFactory`] that will
    /// be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait Line<'model, V: Visitor> {
    /// # type Error;
    /// fn lines(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn lines<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(self, visitor_factory, Self::next_line))
    }

    /// Parse a network and receive a [`Visitor`] that will be driven.
    fn next_network<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse networks as an iterator and receive a [`VisitorFactory`] that will
    /// be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait Network<'model, V: Visitor> {
    /// # type Error;
    /// fn networks(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn networks<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(self, visitor_factory, Self::next_network))
    }

    /// Parse a route and receive a [`Visitor`] that will be driven.
    fn next_route<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse routes as an iterator and receive a [`VisitorFactory`] that will
    /// be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait Route<'model, V: Visitor> {
    /// # type Error;
    /// fn routes(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn routes<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(self, visitor_factory, Self::next_route))
    }

    /// Parse a trip/vehicle journey/scheduled journey and receive a [`Visitor`]
    /// that will be driven.
    fn next_scheduled_journey<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse scheduled journeys as an iterator and receive a [`VisitorFactory`]
    /// that will be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait ScheduledJourney<'model, V: Visitor> {
    /// # type Error;
    /// fn scheduled_journeys(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn scheduled_journeys<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(
            self,
            visitor_factory,
            Self::next_scheduled_journey,
        ))
    }

    /// Parse a stop time/schedule stop and receive a [`Visitor`]  that will be
    /// driven.
    fn next_scheduled_stop<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse scheduled stops as an iterator and receive a [`VisitorFactory`]
    /// that will be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait ScheduledStop<'model, V: Visitor> {
    /// # type Error;
    /// fn scheduled_stops(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn scheduled_stops<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(
            self,
            visitor_factory,
            Self::next_scheduled_stop,
        ))
    }

    /// Parse a stop area and receive a [`Visitor`] that will be driven.
    fn next_stop_area<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse stop areas as an iterator and receive a [`VisitorFactory`] that
    /// will be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait StopArea<'model, V: Visitor> {
    /// # type Error;
    /// fn stop_areas(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn stop_areas<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(self, visitor_factory, Self::next_stop_area))
    }

    /// Parse a stop point and receive a [`Visitor`] that will be driven.
    fn next_stop_point<V>(&mut self, visitor: V) -> Option<Result<V::Value, Self::Error>>
    where
        V: Visitor;

    /// Parse stop points as an iterator and receive a [`VisitorFactory`] that
    /// will be used to produce [`Visitor`] for each iteration.
    ///
    /// # Note
    /// Ideally, the function should be defined like this.
    /// ```compile_fail,E0562
    /// # use fofil_core::Visitor;
    /// # trait StopPoint<'model, V: Visitor> {
    /// # type Error;
    /// fn stop_points(&self) -> impl Iterator<Item = Result<V::Value, Self::Error>> {
    ///      // implementation
    /// #    unimplemented!()
    /// }
    /// # }
    /// ```
    /// but `impl` in return type of trait methods is not yet supported (see
    /// [RFC1522]), hence the complex return type.
    ///
    /// [RFC1522]: https://github.com/rust-lang/rfcs/blob/master/text/1522-conservative-impl-trait.md
    fn stop_points<'parser, V, VF>(
        &'parser mut self,
        visitor_factory: VF,
    ) -> DynIter<'parser, Result<V::Value, Self::Error>>
    where
        V: Visitor + 'parser,
        VF: VisitorFactory<V> + 'parser,
        V::Value: 'parser,
    {
        DynIter::new(ParserIter::new(
            self,
            visitor_factory,
            Self::next_stop_point,
        ))
    }
}
